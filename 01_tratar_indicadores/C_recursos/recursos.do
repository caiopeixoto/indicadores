* IEPS DATA

* Esse codigo trata os dados de recursos

														

********************************************************************************
* 0. Config
********************************************************************************

**** Instala gtools
ssc install gtools

**** Define lista de anos e meses a serem tratados
global meses = `"  "1012" "1112" "1212" "1312" "1412" "1512" "1612" "1712" "1812" "1912"  "2012" "'

**** Setar diretorio
cd "Z:/"


**** Input
global path_lt = "Master/Datasus/raw/CNES/LT/dta/"
global path_pf = "Master/Datasus/raw/CNES/PF/dta/"
global df_healthreg = "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Regioes de Saude.dta"
global df_macrohealthreg = "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Macrorregioes de Saude.dta"
global path_pop "Projects/IEPS Data/Indicadores/Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta"



* Output
global sf_lt_temp = "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/lt_cnes_temp.dta"
global sf_hr_temp = "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/hr_cnes_temp.dta"
global sf_mun_temp_1 = "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_mun_temp_1.dta"
global sf_mun_temp_2 = "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_mun_temp_2.dta"
global sf_brasil = "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_br.dta"
global sf_coduf = "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_uf.dta"
global sf_codmacro = "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_macro.dta"
global sf_codreg = "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_reg.dta"
global sf_codmun = "Projects/IEPS Data/Indicadores/Data/02_temp_data/C_recursos/recursos_mun.dta"



********************************************************************************
* 1. Leitos: Tratar Dados - Abre e Prepara Dados
********************************************************************************
clear
foreach month of global meses {
local files: dir "$path_lt" files "*`month'.dta"
	foreach f of local files {
	di "`f'"
	append using "${path_lt}`f'", keep(CODUFMUN QT_SUS QT_NSUS TP_LEITO CODLEITO COMPETEN)
	}	
}

**** Tira capslock dos nomes das variaveis
rename *, lower

**** Renomeia e transforma codigo de municipio em numerica
destring codufmun, replace
rename codufmun codmun

**** Imputa o código de Brasilia a municípios que são do DF mas com outro codigo de municipio
replace codmun = 530010 if floor(codmun/10000) == 53

**** Gera variavel de ano
gen ano = real(substr(competen,1,4))
drop competen

**** Transforma variaveis em numericas
destring codleito, replace
destring tp_leito, replace

**** Descarta leitos psiquiatricos
//drop if tp_leito==5
drop if codleito == 47

**** Gera variavel de Leitos de UTI do SUS
gen n_leitouti_sus = qt_sus if inrange(codleito,61,63) | ///
inrange(codleito,74,83) | inrange(codleito,85,86)

replace n_leitouti_sus = 0 if n_leitouti_sus==.

**** Gera variavel de Leitos do SUS
gen n_leito_sus=qt_sus

**** Colapsa por municipio/ano
collapse (sum) n_leitouti_sus n_leito_sus, by(codmun ano)

**** Salva base intermediaria
save "${sf_lt_temp}", replace


********************************************************************************
* 2. Recursos Humanos: Tratar Dados - Abre e Prepara Dados
********************************************************************************
**** 
clear
foreach month of global meses {
local files: dir "$path_pf" files "*`month'.dta"
	foreach f of local files {
	di "`f'"
	append using "${path_pf}`f'", keep(CODUFMUN CBO COMPETEN CPF_PROF CPFUNICO HORAOUTR HORAHOSP HORA_AMB)
	}
}

**** Tira capslock dos nomes das variaveis
rename *, lower

**** Renomeia e transforma codigo de municipio em numerica
destring codufmun, replace	
rename codufmun codmun

**** Imputa o codigo de Brasilia a municipios que são do DF mas com código ignorado
replace codmun = 530010 if floor(codmun/10000) == 53

**** Gera variavel de ano
gen ano = real(substr(competen,1,4))
drop competen

**** Organiza variavel que identifica unicamente trabalhadores da saude no CNES
** Note que a ocupacao selecionada para ser unicamente identificada sob CPF_UNICO nao parece ter relacao com horas trabalhadas, ou nenhuma outra variavel na base de dados.
** A ocupacao parece estar sendo aleatoriamente selecionada entre todos os vinculos empregaticios que o individuo possui.
** Isso pode ser um problema ja que o emprego/ocupacao selecionado pode nao ser o principal.
** Iremos usar essa variavel para contar trabalhadores da saude (medicos e enfermeiros), e disponibilizar um outro indicador padronizado por carga horaria.
replace cpfunico = "0" if cpfunico == "NA"
destring cpfunico, replace

**** Conta medicos unicos
gen n_med = 0
replace n_med = 1 if cpfunico == 1 & (inrange(real(cbo),6105,6190) | ///
inrange(real(cbo),223101,223157) | inrange(real(cbo),225103,225350) | ///
inlist(cbo,"2231A1","2231A2","2231F3","2231F4","2231F5") | ///
inlist(cbo,"2231F6","2231F7","2231F8","2231F9","2231G1"))

**** Conta enfermeiros unicos
gen n_enf=0
replace n_enf=1 if cpfunico == 1 & (inrange(real(cbo),7110,7165) | ///
inrange(real(cbo),223505,223565) | inlist(cbo,"2235C1","2235C2","2235C3"))

**** Numero de medicos/enfermeiros padronizado
** Organiza a variavel de horas de trabalho
tab horaoutr if horaoutr >= 100
tab horahosp if horahosp >= 100
tab hora_amb if hora_amb >= 100
** Horas de trabalho estao concentradas em 120, 180, 220, 360 and 440
** Parecem ter um zero a mais
** Portanto, vamos utilizar somente os dois primeiros digitos de seus valores
foreach var in horaoutr horahosp hora_amb { 
	replace `var' = real(substr(string(`var'),1,2))
}
** Gera variavel com o total de horas trabalhadas sob aquela ocupacao
egen totalhr = rowtotal(horaoutr horahosp hora_amb)

** Medicos e enfermeiros, considerando 40 horas de trabalho como padrao
gen n_med_ch = totalhr/40 if (inrange(real(cbo),6105,6190) | ///
inrange(real(cbo),223101,223157) | inrange(real(cbo),225103,225350) | ///
inlist(cbo,"2231A1","2231A2","2231F3","2231F4","2231F5") | ///
inlist(cbo,"2231F6","2231F7","2231F8","2231F9","2231G1"))
gen n_enf_ch = totalhr/40 if (inrange(real(cbo),7110,7165) | ///
inrange(real(cbo),223505,223565) | inlist(cbo,"2235C1","2235C2","2235C3"))

**** Cria variavel auxiliar que da o numero de medicos/enfermeiros unicos por regiao
egen long cpf_prof_num = group(cpf_prof)

gen n_med_reg = .
replace n_med_reg = cpf_prof_num if cpfunico == 1 & (inrange(real(cbo),6105,6190) | ///
inrange(real(cbo),223101,223157) | inrange(real(cbo),225103,225350) | ///
inlist(cbo,"2231A1","2231A2","2231F3","2231F4","2231F5") | ///
inlist(cbo,"2231F6","2231F7","2231F8","2231F9","2231G1"))

gen n_enf_reg = .
replace n_enf_reg = cpf_prof_num if (inrange(real(cbo),7110,7165) | ///
inrange(real(cbo),223505,223565) | inlist(cbo,"2235C1","2235C2","2235C3"))

* Colapsar dados por municipio/ano
gcollapse (sum) n_med n_enf n_med_ch n_enf_ch (nunique) n_med_reg n_enf_reg, by(codmun ano)

save "$sf_hr_temp", replace

********************************************************************************
* 2. Junta bases
********************************************************************************

**** Juntar bases de leitos e de RH
merge m:m codmun ano using "${sf_lt_temp}", nogen

**** Salva primeira base municipal intermediaria
save "$sf_mun_temp_1", replace

**** Junta com base populacional
use "$path_pop", clear
keep ano codmun pop
duplicates drop
merge 1:m codmun ano using "$sf_mun_temp_1"
keep if _merge == 3 | _merge == 1
drop _merge

**** Transforma missings em zero
replace n_leito_sus = 0 if missing(n_leito_sus)
replace n_leitouti_sus = 0 if missing(n_leitouti_sus)

rename codmun ibge
replace ibge = 530010 if ibge >= 529999
merge m:1 ibge using "$df_healthreg", keepusing(regiao) nogen
merge m:1 ibge using "$df_macrohealthreg", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"

**** Inclui legendas para as variaveis
la var codmun "Codigo Municipal"
la var ano "Ano"
la var pop "Populacao"
label var n_leito_sus "Numero total de leitos, SUS"
label var n_leitouti_sus "Numero total de leitos UTI, SUS"
label var n_med "Numero de medicos"
label var n_enf "Numero de enfermeiros"
label var n_med_ch "Numero de medicos padronizados 40h"
label var n_enf_ch "Numero de enfermeiros padronizados 40h"

**** Calcular indicadores de taxas
gen tx_med = 1000*(n_med/pop)
gen tx_enf = 1000*(n_enf/pop)
gen tx_med_ch = 1000*(n_med_ch/pop)
gen tx_enf_ch = 1000*(n_enf_ch/pop)
gen tx_leitouti_sus = 100000*(n_leitouti_sus/pop)
gen tx_leito_sus = 100000*(n_leito_sus/pop)

**** Salva segunda base municipal intermediaria
compress
save "$sf_mun_temp_2", replace



********************************************************************************
* 3. Agrega para niveis geograficos
********************************************************************************

//---------------------------------------------------------------
**** 3.1. Agrega para municipio
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

**** Mantem somente variaveis necessarias
keep codmun ano n_med n_enf n_med_ch n_enf_ch n_leito_sus n_leitouti_sus tx_*

*** Cria linhas com zeros para municipios pequenos que nao tem observacoes
gen byte last = 0
replace last = 1 if ano == 2016 & (codmun == 521205 | codmun == 420325 | codmun == 420555) 
replace last = 1 if (ano == 2018 & codmun == 240160)
ds codmun ano last, not 
quietly foreach v in `r(varlist)' { 
    replace `v' = 0 if last 
}

drop last

**** Coloca como Missing observacoes de Mojui dos Campos. Criado em 2013, ainda nao possuia dados neste ano
gen byte mojui = 0
replace mojui = 1 if ano == 2013 & codmun == 150475
ds codmun ano mojui, not 
quietly foreach v in `r(varlist)' { 
    replace `v' = . if mojui 
}

drop mojui


save "$sf_codmun", replace

//---------------------------------------------------------------
**** 3.2. Agrega para regiao de saude
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

gcollapse (sum) pop n_med n_enf n_med_ch n_enf_ch n_leito_sus n_leitouti_sus (nunique) n_med_reg n_enf_reg, by(regiao ano)

**** Calcular indicadores de taxas
gen tx_med = 1000*(n_med/pop)
gen tx_enf = 1000*(n_enf/pop)
gen tx_med_ch = 1000*(n_med_ch/pop)
gen tx_enf_ch = 1000*(n_enf_ch/pop)
gen tx_leitouti_sus = 100000*(n_leitouti_sus/pop)
gen tx_leito_sus = 100000*(n_leito_sus/pop)

**** Mantem somente variaveis necessarias
keep regiao ano n_med n_enf n_med_ch n_enf_ch n_leito_sus n_leitouti_sus tx_*

save "$sf_codreg", replace

//---------------------------------------------------------------
**** 3.3. Agrega para macrorregiao de saude
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

gcollapse (sum) pop n_med n_enf n_med_ch n_enf_ch n_leito_sus n_leitouti_sus (nunique) n_med_reg n_enf_reg, by(macrorregiao ano)

**** Calcular indicadores de taxas
gen tx_med = 1000*(n_med/pop)
gen tx_enf = 1000*(n_enf/pop)
gen tx_med_ch = 1000*(n_med_ch/pop)
gen tx_enf_ch = 1000*(n_enf_ch/pop)
gen tx_leitouti_sus = 100000*(n_leitouti_sus/pop)
gen tx_leito_sus = 100000*(n_leito_sus/pop)

**** Mantem somente variaveis necessarias
keep macrorregiao ano n_med n_enf n_med_ch n_enf_ch n_leito_sus n_leitouti_sus tx_*

save "$sf_codmacro", replace

//---------------------------------------------------------------
**** 3.4. Agrega para UF
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

gcollapse (sum) pop n_med n_enf n_med_ch n_enf_ch n_leito_sus n_leitouti_sus (nunique) n_med_reg n_enf_reg, by(id_estado ano)

**** Calcular indicadores de taxas
gen tx_med = 1000*(n_med/pop)
gen tx_enf = 1000*(n_enf/pop)
gen tx_med_ch = 1000*(n_med_ch/pop)
gen tx_enf_ch = 1000*(n_enf_ch/pop)
gen tx_leitouti_sus = 100000*(n_leitouti_sus/pop)
gen tx_leito_sus = 100000*(n_leito_sus/pop)

**** Mantem somente variaveis necessarias
keep id_estado ano n_med n_enf n_med_ch n_enf_ch n_leito_sus n_leitouti_sus tx_*

save "$sf_coduf", replace

//---------------------------------------------------------------
**** 3.5. Agrega para Brasil
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

gcollapse (sum) pop n_med n_enf n_med_ch n_enf_ch n_leito_sus n_leitouti_sus (nunique) n_med_reg n_enf_reg, by(ano)

**** Calcular indicadores de taxas
gen tx_med = 1000*(n_med/pop)
gen tx_enf = 1000*(n_enf/pop)
gen tx_med_ch = 1000*(n_med_ch/pop)
gen tx_enf_ch = 1000*(n_enf_ch/pop)
gen tx_leitouti_sus = 100000*(n_leitouti_sus/pop)
gen tx_leito_sus = 100000*(n_leito_sus/pop)

**** Mantem somente variaveis necessarias
keep ano n_med n_enf n_med_ch n_enf_ch n_leito_sus n_leitouti_sus tx_*

save "$sf_brasil", replace
