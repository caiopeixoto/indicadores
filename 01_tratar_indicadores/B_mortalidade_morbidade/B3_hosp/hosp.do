
* IEPS DATA

* Esse código trata os dados de hospitalizacao (SIH) 

														

********************************************************************************
* 0. Config
********************************************************************************
clear

**** Define lista de anos, meses e estados a serem tratados
global anos    = `" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "' // 
global meses   = `" "01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12" "' // 
global estados = `" "AC" "AL" "AM" "AP" "BA" "CE" "DF" "ES" "GO" "MA" "MG" "MS" "MT" "PA" "PB" "PE" "PI" "PR" "RJ" "RN" "RO" "RR" "RS" "SC" "SE" "SP" "TO" "' //  



**** Seta diretorio
cd "Z:/"


**** Input 
global path_sih         "Master/Datasus/raw/SIH/RD/dta/"
global path_pop_idade   "Projects/IEPS Data/Indicadores/Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta" // dados de população por idade
global path_reg_saude   "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Regioes_de_Saude.dta"
global path_macro_saude "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Macrorregioes de Saude.dta"



**** Output 
//global sf_sih_final "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih.dta" 
global sih_temp   "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_temp.dta" 
global sih_mun    "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_mun.dta" 
global sih_reg    "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_reg.dta" 
global sih_macro  "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_macro.dta" 
global sih_uf     "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_uf.dta" 
global sih_br     "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B3_hosp/sih_br.dta" 



**** Programas auxiliares
do "Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/B_mortalidade_morbidade/auxiliar/programa_csap.do"


********************************************************************************
* 1.1 Tratar Dados - Abre e Prepara Dados
********************************************************************************

**** Junta dados por mes/estado/ano e seleciona colunas
foreach ano of global anos {
	foreach month of global meses {
		foreach estado of global estados {
local files: dir "$path_sih" files "*`estado'`ano'`month'.dta"
	foreach f of local files {
	di "`f'"
	append using "${path_sih}`f'", keep(DT_INTER MUNIC_RES DIAG_PRINC IDENT)
}
}
}
}

**** Tira capslock dos nomes das variaveis
rename *, lower

**** Cria variavel de ano
gen ano = substr(dt_inter, 1, 4)
destring ano, replace

drop if ano == 2021 // baixamos dados de 2021 para incorporar hospitalizacoes de 2020 que aparecem apenas nos arquivos de 2021

**** Muda classe da variavel de municipio de residencia
destring munic_res, replace

**** Exclui internacoes + de 1 mes (paciente que continua internado + de 1 mes, ident = 5)
destring ident, replace
drop if ident == 5

**** Imputa o código de Brasilia a municípios que são do DF mas com outro código de municipio
replace munic_res = 530010 if floor(munic_res/10000) == 53

********************************************************************************
* 1.2 Tratar Dados - Define Grupos de Hospitalizacao
********************************************************************************

**** 1. Total Hospitalizacoes
gen byte n_hosp_total = 1


**** 2. Hospitalizacoes por Causas Sensiveis a Atençao Primaria (CSAP)
csap diag_princ n_hosp_csap

**** 3. Hospitalizacoes por Causas Mal Definidas
**** classificacao segue  https://www.who.int/data/gho/indicator-metadata-registry/imr-details/1215
*gen byte n_hosp_maldef = 1 if match(diag_princ, "R*")




********************************************************************************
* 1.3 Junta bases e agrega para municipio/ano
********************************************************************************

**** Renomeia variavel de codigo municipal e altera seu formato
rename munic_res codmun
destring codmun, replace

**** Agrupa no nivel do municipio e ano
collapse (sum) n_hosp* , by(codmun ano)

**** Faz o merge com dados de unidades geograficas e de população por idade 
rename codmun ibge
merge m:1 ibge using "$path_reg_saude", keepusing(regiao) nogen
merge m:1 ibge using "$path_macro_saude", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"
merge m:1 codmun ano using "${path_pop_idade}", 
keep if _merge==3
drop _merge

* Save temp	
compress
save "$sih_temp", replace

**** Cria taxas 
 foreach var in total csap  {
	
	gen tx_hosp_`var' = 100000*(n_hosp_`var'/pop)
	
 }
 
  
**** Muda nome de taxa de hosp totais
rename tx_hosp_total tx_hosp
rename n_hosp_total n_hosp

**** Proporcao de hosp mal definidas
*gen pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp)
 
**** Seleciona variaveis
keep codmun ano n_hosp n_hosp_csap  tx_hosp tx_hosp_csap 

**** Ordenar colunas
order codmun ano 

**** Salvar
save "${sih_mun}", replace




********************************************************************************
* 1.4 Agrega para demais niveis geograficos
********************************************************************************

**** Agrega para regiao de saude
use "$sih_temp", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) n_hosp* pop*, by(regiao ano)
**** Cria taxas 
 foreach var in total csap  {
	gen tx_hosp_`var' = 100000*(n_hosp_`var'/pop)
 }
**** Muda nome de taxa de hosp totais
rename tx_hosp_total tx_hosp
rename n_hosp_total n_hosp
**** Proporcao de hosp mal definidas
*gen pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp)
**** Seleciona variaveis
keep regiao ano n_hosp n_hosp_csap  tx_hosp tx_hosp_csap 
**** Ordenar colunas
order regiao ano 
**** Salvar
save "${sih_reg}", replace



**** Agrega para macrorregiao de saude
use "$sih_temp", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) n_hosp* pop*, by(macrorregiao ano)
**** Cria taxas 
 foreach var in total csap  {
	gen tx_hosp_`var' = 100000*(n_hosp_`var'/pop)	
 }
**** Muda nome de taxa de hosp totais
rename tx_hosp_total tx_hosp
rename n_hosp_total n_hosp
**** Proporcao de hosp mal definidas
*gen pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp)
**** Seleciona variaveis
keep macrorregiao ano n_hosp n_hosp_csap  tx_hosp tx_hosp_csap 
**** Ordenar colunas
order macrorregiao ano 
**** Salvar
save "${sih_macro}", replace



**** Agrega para UF
use "$sih_temp", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) n_hosp* pop*, by(id_estado ano)
**** Cria taxas 
 foreach var in total csap  {
	gen tx_hosp_`var' = 100000*(n_hosp_`var'/pop)
 }
**** Muda nome de taxa de hosp totais
rename tx_hosp_total tx_hosp
rename n_hosp_total n_hosp
**** Proporcao de hosp mal definidas
*gen pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp)
**** Seleciona variaveis
keep id_estado ano n_hosp n_hosp_csap  tx_hosp tx_hosp_csap 
**** Ordenar colunas
order id_estado ano 
**** Salvar
save "${sih_uf}", replace



**** Agrega para Brasil
use "$sih_temp", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) n_hosp* pop*, by(ano)
**** Cria taxas 
 foreach var in total csap  {
	gen tx_hosp_`var' = 100000*(n_hosp_`var'/pop)
 }
**** Muda nome de taxa de hosp totais
rename tx_hosp_total tx_hosp
rename n_hosp_total n_hosp
**** Proporcao de hosp mal definidas
*gen pct_hosp_maldef = 100*(n_hosp_maldef/n_hosp)
**** Seleciona variaveis
keep ano n_hosp n_hosp_csap  tx_hosp tx_hosp_csap 
**** Ordenar colunas
order  ano 
**** Salvar
save "${sih_br}", replace
