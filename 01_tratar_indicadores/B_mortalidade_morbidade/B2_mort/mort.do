
* IEPS DATA

* Esse código trata os dados de mortalidade (SIM)	 

														

********************************************************************************
* 0. Config
********************************************************************************
clear

**** Define lista de anos e estados a serem tratados
global anos = `" "2010" "2011"  "2012" "2013" "2014" "2015" "2016" "2017" "2018" "2019"  "2020"  "' // 
global estados = `" "AC" "AL" "AM" "AP" "BA" "CE" "DF" "ES" "GO" "MA" "MG" "MS" "MT" "PA" "PB" "PE" "PI" "PR" "RJ" "RN" "RO" "RR" "RS" "SC" "SE" "SP" "TO" "'


**** Setar diretorio
cd "Z:/"

**** Input
global path_sim "Master/Datasus/raw/SIM/DO/dta/" // dados de mortalidade

global path_pop_idade "Projects/IEPS Data/Indicadores/Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta" // dados de população por idade
global path_reg_saude   "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Regioes_de_Saude.dta"
global path_macro_saude "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Macrorregioes de Saude.dta"

**** Output
global sim_temp "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B2_mort/sim_temp.dta" 
global sim_mun_adj "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B2_mort/sim_mun_adj.dta" 
global sim_reg_adj "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B2_mort/sim_reg_adj.dta" 
global sim_macro_adj "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B2_mort/sim_macro_adj.dta" 
global sim_uf_adj "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B2_mort/sim_uf_adj.dta" 
global sim_br_adj "Projects/IEPS Data/Indicadores/Data/02_temp_data/B_morbidade_mortalidade/B2_mort/sim_br_adj.dta" 

**** Programas auxiliares 
do "Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/B_mortalidade_morbidade/auxiliar/programa_csap.do"
do "Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/B_mortalidade_morbidade/auxiliar/programa_evitaveis.do"


********************************************************************************
* 1.1 Tratar Dados - Abre e Prepara Dados
********************************************************************************

**** Junta dados por estado/ano e seleciona colunas
clear
foreach ano of global anos {
    foreach estado of global estados {
local files: dir "$path_sim" files "*`estado'`ano'.dta"
	foreach f of local files {
	di "`f'"
	append using "${path_sim}`f'", keep(DTOBITO CODMUNRES CAUSABAS IDADE SEXO)
}
}
}

**** Tira capslock dos nomes das variaveis
rename *, lower

		
**** Cria variavel de ano
gen ano=real(substr(dtobito,-4,4))
label variable ano "ano do óbito"


**** Muda classe da variável de idade
destring idade, replace force


**** Cria grupos etários (faixa etária 2 TabNet)
gen byte n_obitos_0a4 = 1 if (idade<=404)
gen byte n_obitos_5a9 = 1 if (idade>=405 & idade<=409)
gen byte n_obitos_10a14 = 1 if (idade>=410 & idade<=414) 
gen byte n_obitos_15a19 = 1 if (idade>=415 & idade<=419)
gen byte n_obitos_20a24 = 1 if (idade>=420 & idade<=424)
gen byte n_obitos_25a29 = 1 if (idade>=425 & idade<=429)
gen byte n_obitos_30a34 = 1 if (idade>=430 & idade<=434)
gen byte n_obitos_35a39 = 1 if (idade>=435 & idade<=439)
gen byte n_obitos_40a44 = 1 if (idade>=440 & idade<=444)
gen byte n_obitos_45a49 = 1 if (idade>=445 & idade<=449)
gen byte n_obitos_50a54 = 1 if (idade>=450 & idade<=454)
gen byte n_obitos_55a59 = 1 if (idade>=455 & idade<=459)
gen byte n_obitos_60a64 = 1 if (idade>=460 & idade<=464)
gen byte n_obitos_65a69 = 1 if (idade>=465 & idade<=469)
gen byte n_obitos_70a74 = 1 if (idade>=470 & idade<=474)
gen byte n_obitos_75a79 = 1 if (idade>=475 & idade<=479)
gen byte n_obitos_80m = 1 if (idade>=480)

*gen byte n_obitos_80a84 = 1 if (idade>=480 & idade<=484)
*gen byte n_obitos_85a89 = 1 if (idade>=485 & idade<=489)
*gen byte n_obitos_90a95 = 1 if (idade>=490 & idade<=494)
*gen byte n_obitos_95a99 = 1 if (idade>=495 & idade<=499)
*gen byte n_obitos_100mais = 1 if (idade>=500)



**** Inclui Legenda para variáveis
label var causabas "causa basica de obito, segundo CID-10" 
label var codmunres "municipio de residencia do falecido"

**** Descarta  variável da data de óbito
drop dtobito



********************************************************************************
* 1.2 Tratar Dados - Define Grupos de Mortalidade
********************************************************************************

**** 1. Total de óbitos
gen byte n_obitos_total = 1


**** 2. Óbitos Infantis
*gen n_obitosinf=0
*destring idade, replace force
*replace n_obitosinf=1 if idade < 401
*label variable n_obitosinf "dummy for infant death (< 1 year)"

/* Idade (Age) codebook - Indicates Age in hours
267  Ignorado                                           000-999
      1  < 1 hour                                       001-100
      2  1-23 hours                                     101-123
     25  < 1 day, hours ignored                         200
     26  1-29 days                                      201-229
     55  < 1 month, days ignored                        300
     56  1-11 months                                    301-311
     67  less than 1 year ignored                       400
     68  1-99 years                                     401-499
    167  100 years                                      500
    168  101-199 years                                  501-599 */
	

**** 3. Obitos por Causas Sensiveis a Atençao Primaria (CSAP)
**** Aciona programa que identifica obitos por CSAP
csap causabas n_obitos_csap

**** 4. Obitos Evitáveis
**** CID-10: http://apps.who.int/classifications/icd10/browse/2016/
**** Classificaçao baseada em Nolte & McKee (2003, 2008, 2011)
*** Nolte E, McKee CM. Measuring the health of nations: updating an earlier analysis. Health Aff (Millwood) 2008;27:58-71.
*** Nolte E, McKee M. Measuring the health of nations: analysis of mortality amenable to health care. Bmj 2003;327:1129.
*** Nolte E, McKee M. Variations in amenable mortality--trends in 16 high-income nations. Health Policy 2011;103:47-52.


**** Aciona programa que identifica óbitos por causas evitaveis
evitaveis causabas n_obitos_evit idade
*replace n_obitos_evit = 0 if missing(n_obitos_evit)

**** 5. Óbitos por Causas Mal Definidas
**** classificacao segue  https://www.who.int/data/gho/indicator-metadata-registry/imr-details/1215
gen byte n_obitos_maldef = 1 if match(causabas, "R*")
replace n_obitos_maldef = 0 if missing(n_obitos_maldef)

**** Descarta variável da idade
drop idade

///


**** Cria indicador por idade para cada grupo de doenças definido
foreach var in total csap evit maldef {

	foreach idade in 0a4 5a9 10a14 15a19 20a24 25a29 30a34 35a39 40a44 45a49 50a54 55a59 60a64 65a69 70a74 75a79 80m {	
		
	gen byte n_obitos_`var'_`idade' = n_obitos_`var'*n_obitos_`idade'

		
	}
}

*** Descarta variavel de causa basica do obito
drop causabas 


********************************************************************************
* 1.4 Tratar Dados - Agrupa no nivel municipio ano
********************************************************************************

destring codmunres, replace
rename codmunres codmun

**** Imputa o código de Brasilia a municípios que são do DF mas com código ignorado
replace codmun = 530010 if floor(codmun/10000) == 53



**** Agrupa dados no nível de município-ano
collapse (sum) n_obitos*, by(codmun ano)

**** Faz o merge com dados de unidades geograficas e de população por idade 
rename codmun ibge
merge m:1 ibge using "$path_reg_saude", keepusing(regiao) nogen
merge m:1 ibge using "$path_macro_saude", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"
merge m:1 codmun ano using "${path_pop_idade}"
keep if _merge==3
drop _merge

* Save temp	
compress
save "$sim_temp", replace
		

**** Cria categoria de referência para ajuste etário baseada em numeros de 2010
ds pop*
di `r(varlist)'

foreach var in `r(varlist)' {
	bys ano: egen `var'_br = sum(`var')
	gen `var'_br_ref = `var'_br if ano == 2010
	replace `var'_br_ref = `var'_br_ref[_n-1] if missing(`var'_br_ref) 

}							 
	
**** Cria grupos etários (para ajuste OMS)
**** https://www.who.int/healthinfo/paper31.pdf (pag 12)
gen byte pop0a4_oms =  8.86
gen byte pop5a9_oms =  8.69
gen byte pop10a14_oms =  8.6
gen byte pop15a19_oms =  8.47
gen byte pop20a24_oms =  8.22
gen byte pop25a29_oms =  7.93
gen byte pop30a34_oms =  7.61
gen byte pop35a39_oms =  7.15
gen byte pop40a44_oms =  6.59
gen byte pop45a49_oms =  6.04
gen byte pop50a54_oms =  5.37
gen byte pop55a59_oms =  4.55
gen byte pop60a64_oms =  3.72
gen byte pop65a69_oms =  2.96
gen byte pop70a74_oms =  2.21
gen byte pop75a79_oms =  1.52
gen byte pop80a84_oms =  0.91
gen byte pop85a89_oms =  0.44
gen byte pop90a94_oms =  0.15
gen byte pop95a99_oms =  0.04
gen byte pop100mais_oms =    0.005
gen byte pop80m_oms   = pop80a84_oms + pop85a89_oms + pop90a94_oms + pop95a99_oms + pop100mais_oms


	
* Ajuste etario
foreach var in total csap evit  {
	
	gen tx_mort_`var' = 100000*(n_obitos_`var'/pop)
	
	gen tx_mort_`var'_aj_oms = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_oms/100)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_oms/100)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_oms/100)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_oms/100)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_oms/100)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_oms/100)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_oms/100)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_oms/100)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_oms/100)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_oms/100)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_oms/100)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_oms/100)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_oms/100)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_oms/100)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_oms/100) + (n_obitos_`var'_75a79/pop75a79)*(pop75a79_oms/100) + (n_obitos_`var'_80m/pop80m)*(pop80m_oms/100))
	
	gen tx_mort_`var'_aj_cens = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_br_ref/pop_br_ref)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_br_ref/pop_br_ref)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_br_ref/pop_br_ref)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_br_ref/pop_br_ref)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_br_ref/pop_br_ref)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_br_ref/pop_br_ref)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_br_ref/pop_br_ref)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_br_ref/pop_br_ref)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_br_ref/pop_br_ref)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_br_ref/pop_br_ref)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_br_ref/pop_br_ref)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_br_ref/pop_br_ref)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_br_ref/pop_br_ref)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_br_ref/pop_br_ref)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_br_ref/pop_br_ref)+ (n_obitos_`var'_75a79/pop75a79)*(pop75a79_br_ref/pop_br_ref) + (n_obitos_`var'_80m/pop80m)*(pop80m/pop_br_ref))

	
}
 
**** Muda nome de taxa de obitos totais
rename tx_mort_total tx_mort
rename n_obitos_total n_obitos
rename tx_mort_total_aj_oms tx_mort_aj_oms
rename tx_mort_total_aj_cens tx_mort_aj_cens
 
* proporcao de obitos mal definidas
gen pct_mort_maldef = 100*(n_obitos_maldef/n_obitos)
 
**** Seleciona variaveis a serem exportadas
cap drop pop* 
keep codmun ano n_obitos n_obitos_csap n_obitos_evit n_obitos_maldef tx_mort tx_mort_aj_oms tx_mort_aj_cens tx_mort_csap tx_mort_csap_aj_oms tx_mort_csap_aj_cens tx_mort_evit     tx_mort_evit_aj_oms  tx_mort_evit_aj_cens pct_mort_maldef


**** Cria linhas com zeros para municipios pequenos que nao tem observacoes
gen byte last = _n == _N 
expand 3 if last 

replace ano = 2011 if _n == _N 
replace codmun = 350720 if _n == _N // Bora (SP)
replace ano = 2011 if _n == _N-1 
replace codmun = 430692 if _n == _N-1 // Engenho Velho (RS)

replace last = _n == _N 
ds codmun ano, not 
quietly foreach v in `r(varlist)' { 
    replace `v' = 0 if last 
}

replace last = _n == _N -1
ds codmun ano, not 
quietly foreach v in `r(varlist)' { 
    replace `v' = 0 if last 
}

drop last


**** Ordena vars por mun ano
order codmun ano 

**** Salva
save "${sim_mun_adj}", replace 




********************************************************************************
* 1.4 Tratar Dados - Agrupa no nivel regiao ano
********************************************************************************

use "$sim_temp", clear

**** Agrupa dados no nível de regiao-ano
collapse (sum) n_obitos* pop*, by(regiao ano)


**** Cria categoria de referencia para ajuste etário baseada em numeros de 2010	
ds pop*
di `r(varlist)'

foreach var in `r(varlist)' {
	bys ano: egen `var'_br = sum(`var')
	gen `var'_br_ref = `var'_br if ano == 2010
	replace `var'_br_ref = `var'_br_ref[_n-1] if missing(`var'_br_ref) 

}							 
	
**** Cria grupos etarios (para ajuste OMS)
**** https://www.who.int/healthinfo/paper31.pdf (pag 12)
gen byte pop0a4_oms =  8.86
gen byte pop5a9_oms =  8.69
gen byte pop10a14_oms =  8.6
gen byte pop15a19_oms =  8.47
gen byte pop20a24_oms =  8.22
gen byte pop25a29_oms =  7.93
gen byte pop30a34_oms =  7.61
gen byte pop35a39_oms =  7.15
gen byte pop40a44_oms =  6.59
gen byte pop45a49_oms =  6.04
gen byte pop50a54_oms =  5.37
gen byte pop55a59_oms =  4.55
gen byte pop60a64_oms =  3.72
gen byte pop65a69_oms =  2.96
gen byte pop70a74_oms =  2.21
gen byte pop75a79_oms =  1.52
gen byte pop80a84_oms =  0.91
gen byte pop85a89_oms =  0.44
gen byte pop90a94_oms =  0.15
gen byte pop95a99_oms =  0.04
gen byte pop100mais_oms =    0.005
gen byte pop80m_oms   = pop80a84_oms + pop85a89_oms + pop90a94_oms + pop95a99_oms + pop100mais_oms
	

* Auste etario
foreach var in total csap evit  {
	
	gen tx_mort_`var' = 100000*(n_obitos_`var'/pop)
	
	gen tx_mort_`var'_aj_oms = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_oms/100)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_oms/100)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_oms/100)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_oms/100)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_oms/100)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_oms/100)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_oms/100)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_oms/100)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_oms/100)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_oms/100)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_oms/100)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_oms/100)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_oms/100)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_oms/100)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_oms/100) + (n_obitos_`var'_75a79/pop75a79)*(pop75a79_oms/100) + (n_obitos_`var'_80m/pop80m)*(pop80m_oms/100))
	
	gen tx_mort_`var'_aj_cens = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_br_ref/pop_br_ref)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_br_ref/pop_br_ref)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_br_ref/pop_br_ref)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_br_ref/pop_br_ref)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_br_ref/pop_br_ref)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_br_ref/pop_br_ref)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_br_ref/pop_br_ref)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_br_ref/pop_br_ref)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_br_ref/pop_br_ref)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_br_ref/pop_br_ref)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_br_ref/pop_br_ref)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_br_ref/pop_br_ref)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_br_ref/pop_br_ref)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_br_ref/pop_br_ref)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_br_ref/pop_br_ref)+ (n_obitos_`var'_75a79/pop75a79)*(pop75a79_br_ref/pop_br_ref) + (n_obitos_`var'_80m/pop80m)*(pop80m/pop_br_ref))

	
}
 
**** Muda nome de taxa de obitos totais
rename tx_mort_total tx_mort
rename n_obitos_total n_obitos
rename tx_mort_total_aj_oms tx_mort_aj_oms
rename tx_mort_total_aj_cens tx_mort_aj_cens
 
* proporcao de obitos mal definidas
gen pct_mort_maldef = 100*(n_obitos_maldef/n_obitos)
 
**** Seleciona variaveis a serem exportadas
cap drop pop* 
keep regiao ano n_obitos n_obitos_csap n_obitos_evit n_obitos_maldef tx_mort tx_mort_aj_oms tx_mort_aj_cens tx_mort_csap tx_mort_csap_aj_oms tx_mort_csap_aj_cens tx_mort_evit     tx_mort_evit_aj_oms  tx_mort_evit_aj_cens pct_mort_maldef


**** Ordena vars por mun ano
order regiao ano 

**** Salva
save "${sim_reg_adj}", replace 



********************************************************************************
* 1.4 Tratar Dados - Agrupa no nivel macrorregiao ano
********************************************************************************
use "$sim_temp", clear

**** Agrupa dados no nível de município-ano
collapse (sum) n_obitos* pop*, by(macrorregiao ano)

**** Cria categoria de referência para ajuste etário baseada em numeros de 2010
ds pop*
di `r(varlist)'

foreach var in `r(varlist)' {
	bys ano: egen `var'_br = sum(`var')
	gen `var'_br_ref = `var'_br if ano == 2010
	replace `var'_br_ref = `var'_br_ref[_n-1] if missing(`var'_br_ref) 

}							 
	
**** Cria grupos etários (para ajuste OMS)
**** https://www.who.int/healthinfo/paper31.pdf (pag 12)
gen byte pop0a4_oms =  8.86
gen byte pop5a9_oms =  8.69
gen byte pop10a14_oms =  8.6
gen byte pop15a19_oms =  8.47
gen byte pop20a24_oms =  8.22
gen byte pop25a29_oms =  7.93
gen byte pop30a34_oms =  7.61
gen byte pop35a39_oms =  7.15
gen byte pop40a44_oms =  6.59
gen byte pop45a49_oms =  6.04
gen byte pop50a54_oms =  5.37
gen byte pop55a59_oms =  4.55
gen byte pop60a64_oms =  3.72
gen byte pop65a69_oms =  2.96
gen byte pop70a74_oms =  2.21
gen byte pop75a79_oms =  1.52
gen byte pop80a84_oms =  0.91
gen byte pop85a89_oms =  0.44
gen byte pop90a94_oms =  0.15
gen byte pop95a99_oms =  0.04
gen byte pop100mais_oms =    0.005
gen byte pop80m_oms   = pop80a84_oms + pop85a89_oms + pop90a94_oms + pop95a99_oms + pop100mais_oms
	

* Auste etario
foreach var in total csap evit  {
	
	gen tx_mort_`var' = 100000*(n_obitos_`var'/pop)
	
	gen tx_mort_`var'_aj_oms = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_oms/100)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_oms/100)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_oms/100)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_oms/100)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_oms/100)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_oms/100)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_oms/100)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_oms/100)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_oms/100)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_oms/100)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_oms/100)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_oms/100)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_oms/100)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_oms/100)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_oms/100) + (n_obitos_`var'_75a79/pop75a79)*(pop75a79_oms/100) + (n_obitos_`var'_80m/pop80m)*(pop80m_oms/100))
	
	gen tx_mort_`var'_aj_cens = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_br_ref/pop_br_ref)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_br_ref/pop_br_ref)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_br_ref/pop_br_ref)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_br_ref/pop_br_ref)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_br_ref/pop_br_ref)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_br_ref/pop_br_ref)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_br_ref/pop_br_ref)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_br_ref/pop_br_ref)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_br_ref/pop_br_ref)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_br_ref/pop_br_ref)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_br_ref/pop_br_ref)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_br_ref/pop_br_ref)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_br_ref/pop_br_ref)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_br_ref/pop_br_ref)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_br_ref/pop_br_ref)+ (n_obitos_`var'_75a79/pop75a79)*(pop75a79_br_ref/pop_br_ref) + (n_obitos_`var'_80m/pop80m)*(pop80m/pop_br_ref))

	
}
 
**** Muda nome de taxa de obitos totais
rename tx_mort_total tx_mort
rename n_obitos_total n_obitos
rename tx_mort_total_aj_oms tx_mort_aj_oms
rename tx_mort_total_aj_cens tx_mort_aj_cens
 
* proporcao de obitos mal definidas
gen pct_mort_maldef = 100*(n_obitos_maldef/n_obitos)
 
**** Seleciona variaveis a serem exportadas
cap drop pop* 
keep macrorregiao ano n_obitos n_obitos_csap n_obitos_evit n_obitos_maldef tx_mort tx_mort_aj_oms tx_mort_aj_cens tx_mort_csap tx_mort_csap_aj_oms tx_mort_csap_aj_cens tx_mort_evit     tx_mort_evit_aj_oms  tx_mort_evit_aj_cens pct_mort_maldef

**** Ordena vars por mun ano
order macrorregiao ano 

**** Salva
save "${sim_macro_adj}", replace 




********************************************************************************
* 1.4 Tratar Dados - Agrupa no nivel estado ano
********************************************************************************

use "$sim_temp", clear

**** Agrupa dados no nível de município-ano
collapse (sum) n_obitos* pop*, by(id_estado ano)


**** Cria categoria de referência para ajuste etário baseada em numeros de 2010
ds pop*
di `r(varlist)'

foreach var in `r(varlist)' {
	bys ano: egen `var'_br = sum(`var')
	gen `var'_br_ref = `var'_br if ano == 2010
	replace `var'_br_ref = `var'_br_ref[_n-1] if missing(`var'_br_ref) 

}							 
	
**** Cria grupos etários (para ajuste OMS)
**** https://www.who.int/healthinfo/paper31.pdf (pag 12)
gen byte pop0a4_oms =  8.86
gen byte pop5a9_oms =  8.69
gen byte pop10a14_oms =  8.6
gen byte pop15a19_oms =  8.47
gen byte pop20a24_oms =  8.22
gen byte pop25a29_oms =  7.93
gen byte pop30a34_oms =  7.61
gen byte pop35a39_oms =  7.15
gen byte pop40a44_oms =  6.59
gen byte pop45a49_oms =  6.04
gen byte pop50a54_oms =  5.37
gen byte pop55a59_oms =  4.55
gen byte pop60a64_oms =  3.72
gen byte pop65a69_oms =  2.96
gen byte pop70a74_oms =  2.21
gen byte pop75a79_oms =  1.52
gen byte pop80a84_oms =  0.91
gen byte pop85a89_oms =  0.44
gen byte pop90a94_oms =  0.15
gen byte pop95a99_oms =  0.04
gen byte pop100mais_oms =    0.005
gen byte pop80m_oms   = pop80a84_oms + pop85a89_oms + pop90a94_oms + pop95a99_oms + pop100mais_oms
	

* Auste etario
foreach var in total csap evit  {
	
	gen tx_mort_`var' = 100000*(n_obitos_`var'/pop)
	
	gen tx_mort_`var'_aj_oms = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_oms/100)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_oms/100)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_oms/100)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_oms/100)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_oms/100)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_oms/100)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_oms/100)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_oms/100)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_oms/100)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_oms/100)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_oms/100)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_oms/100)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_oms/100)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_oms/100)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_oms/100) + (n_obitos_`var'_75a79/pop75a79)*(pop75a79_oms/100) + (n_obitos_`var'_80m/pop80m)*(pop80m_oms/100))
	
	gen tx_mort_`var'_aj_cens = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_br_ref/pop_br_ref)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_br_ref/pop_br_ref)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_br_ref/pop_br_ref)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_br_ref/pop_br_ref)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_br_ref/pop_br_ref)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_br_ref/pop_br_ref)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_br_ref/pop_br_ref)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_br_ref/pop_br_ref)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_br_ref/pop_br_ref)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_br_ref/pop_br_ref)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_br_ref/pop_br_ref)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_br_ref/pop_br_ref)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_br_ref/pop_br_ref)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_br_ref/pop_br_ref)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_br_ref/pop_br_ref)+ (n_obitos_`var'_75a79/pop75a79)*(pop75a79_br_ref/pop_br_ref) + (n_obitos_`var'_80m/pop80m)*(pop80m/pop_br_ref))

	
}
 
**** Muda nome de taxa de obitos totais
rename tx_mort_total tx_mort
rename n_obitos_total n_obitos
rename tx_mort_total_aj_oms tx_mort_aj_oms
rename tx_mort_total_aj_cens tx_mort_aj_cens
 
* proporcao de obitos mal definidas
gen pct_mort_maldef = 100*(n_obitos_maldef/n_obitos)
 
**** Seleciona variaveis a serem exportadas
cap drop pop* 
keep id_estado ano n_obitos n_obitos_csap n_obitos_evit n_obitos_maldef tx_mort tx_mort_aj_oms tx_mort_aj_cens tx_mort_csap tx_mort_csap_aj_oms tx_mort_csap_aj_cens tx_mort_evit     tx_mort_evit_aj_oms  tx_mort_evit_aj_cens pct_mort_maldef

**** Ordena vars por mun ano
order id_estado ano 

**** Salva
save "${sim_uf_adj}", replace 



********************************************************************************
* 1.4 Tratar Dados - Agrupa no nivel nacional ano
********************************************************************************

use "$sim_temp", clear

**** Agrupa dados no nível de município-ano
collapse (sum) n_obitos* pop*, by(ano)


**** Cria categoria de referência para ajuste etário baseada em numeros de 2010
ds pop*
di `r(varlist)'

foreach var in `r(varlist)' {
	bys ano: egen `var'_br = sum(`var')
	gen `var'_br_ref = `var'_br if ano == 2010
	replace `var'_br_ref = `var'_br_ref[_n-1] if missing(`var'_br_ref) 

}							 
	
**** Cria grupos etários (para ajuste OMS)
**** https://www.who.int/healthinfo/paper31.pdf (pag 12)
gen byte pop0a4_oms =  8.86
gen byte pop5a9_oms =  8.69
gen byte pop10a14_oms =  8.6
gen byte pop15a19_oms =  8.47
gen byte pop20a24_oms =  8.22
gen byte pop25a29_oms =  7.93
gen byte pop30a34_oms =  7.61
gen byte pop35a39_oms =  7.15
gen byte pop40a44_oms =  6.59
gen byte pop45a49_oms =  6.04
gen byte pop50a54_oms =  5.37
gen byte pop55a59_oms =  4.55
gen byte pop60a64_oms =  3.72
gen byte pop65a69_oms =  2.96
gen byte pop70a74_oms =  2.21
gen byte pop75a79_oms =  1.52
gen byte pop80a84_oms =  0.91
gen byte pop85a89_oms =  0.44
gen byte pop90a94_oms =  0.15
gen byte pop95a99_oms =  0.04
gen byte pop100mais_oms =    0.005
gen byte pop80m_oms   = pop80a84_oms + pop85a89_oms + pop90a94_oms + pop95a99_oms + pop100mais_oms
	

* Ajuste etario
foreach var in total csap evit  {
	
	gen tx_mort_`var' = 100000*(n_obitos_`var'/pop)
	
	gen tx_mort_`var'_aj_oms = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_oms/100)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_oms/100)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_oms/100)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_oms/100)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_oms/100)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_oms/100)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_oms/100)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_oms/100)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_oms/100)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_oms/100)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_oms/100)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_oms/100)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_oms/100)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_oms/100)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_oms/100) + (n_obitos_`var'_75a79/pop75a79)*(pop75a79_oms/100) + (n_obitos_`var'_80m/pop80m)*(pop80m_oms/100))
	
	gen tx_mort_`var'_aj_cens = 100000*((n_obitos_`var'_0a4/pop0a4)*(pop0a4_br_ref/pop_br_ref)+ (n_obitos_`var'_5a9/pop5a9)*(pop5a9_br_ref/pop_br_ref)+ (n_obitos_`var'_10a14/pop10a14)*(pop10a14_br_ref/pop_br_ref)+ (n_obitos_`var'_15a19/pop15a19)*(pop15a19_br_ref/pop_br_ref)+ (n_obitos_`var'_20a24/pop20a24)*(pop20a24_br_ref/pop_br_ref)+ (n_obitos_`var'_25a29/pop25a29)*(pop25a29_br_ref/pop_br_ref)+ (n_obitos_`var'_30a34/pop30a34)*(pop30a34_br_ref/pop_br_ref)+ (n_obitos_`var'_35a39/pop35a39)*(pop35a39_br_ref/pop_br_ref)+ (n_obitos_`var'_40a44/pop40a44)*(pop40a44_br_ref/pop_br_ref)+ (n_obitos_`var'_45a49/pop45a49)*(pop45a49_br_ref/pop_br_ref)+ (n_obitos_`var'_50a54/pop50a54)*(pop50a54_br_ref/pop_br_ref)+ (n_obitos_`var'_55a59/pop55a59)*(pop55a59_br_ref/pop_br_ref)+ (n_obitos_`var'_60a64/pop60a64)*(pop60a64_br_ref/pop_br_ref)+ (n_obitos_`var'_65a69/pop65a69)*(pop65a69_br_ref/pop_br_ref)+ (n_obitos_`var'_70a74/pop70a74)*(pop70a74_br_ref/pop_br_ref)+ (n_obitos_`var'_75a79/pop75a79)*(pop75a79_br_ref/pop_br_ref) + (n_obitos_`var'_80m/pop80m)*(pop80m/pop_br_ref))

	
}
 
**** Muda nome de taxa de obitos totais
rename tx_mort_total tx_mort
rename n_obitos_total n_obitos
rename tx_mort_total_aj_oms tx_mort_aj_oms
rename tx_mort_total_aj_cens tx_mort_aj_cens
 
* proporcao de obitos mal definidas
gen pct_mort_maldef = 100*(n_obitos_maldef/n_obitos)
 
**** Seleciona variaveis a serem exportadas
cap drop pop* 
keep  ano n_obitos n_obitos_csap n_obitos_evit n_obitos_maldef tx_mort tx_mort_aj_oms tx_mort_aj_cens tx_mort_csap tx_mort_csap_aj_oms tx_mort_csap_aj_cens tx_mort_evit     tx_mort_evit_aj_oms  tx_mort_evit_aj_cens pct_mort_maldef

**** Ordena vars por mun ano
order ano 

**** Salva
save "${sim_br_adj}", replace 