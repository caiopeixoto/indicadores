
*** Cria Programa para classificar causas sensiveis a atencao primaria

*** Definicao segundo Ministerio da Saude:
*** http://bvsms.saude.gov.br/bvs/saudelegis/sas/2008/prt0221_17_04_2008.html

capture program drop csap
program define csap
	
	args var_cid new_var 
	
	{
		    
			gen byte `new_var' = 1 if match(`var_cid', "A0*") == 1
replace `new_var' = 1 if match(`var_cid', "A170*") | match(`var_cid', "A18*") |  match(`var_cid', "A19*") == 1
replace `new_var' = 1 if match(`var_cid', "A150*") | match(`var_cid', "A151*") | match(`var_cid', "A152*") | match(`var_cid', "A153*") | match(`var_cid', "A160*") | match(`var_cid', "A161*") | match(`var_cid', "A162*") == 1
replace `new_var' = 1 if match(`var_cid', "A154*") | match(`var_cid', "A155*") | match(`var_cid', "A156*") | match(`var_cid', "A157*") | match(`var_cid', "A158*") | match(`var_cid', "A159*") == 1
replace `new_var' = 1 if match(`var_cid', "A163*") | match(`var_cid', "A164*") | match(`var_cid', "A165*") | match(`var_cid', "A166*") | match(`var_cid', "A167*") | match(`var_cid', "A168*") | match(`var_cid', "A169*") == 1
replace `new_var' = 1 if match(`var_cid', "A171*") | match(`var_cid', "A172*") | match(`var_cid', "A173*") | match(`var_cid', "A174*") | match(`var_cid', "A175*") | ///
							match(`var_cid', "A176*") | match(`var_cid', "A177*") | match(`var_cid', "A178*") | match(`var_cid', "A179*") == 1
replace `new_var' = 1 if match(`var_cid', "A36*") | match(`var_cid', "A37*") | match(`var_cid', "A33*") | match(`var_cid', "A34*") | match(`var_cid', "A35*") == 1
replace `new_var' = 1 if match(`var_cid', "A46*") | match(`var_cid', "A50*") | match(`var_cid', "A51*") | match(`var_cid', "A52*") | match(`var_cid', "A53*") == 1
replace `new_var' = 1 if match(`var_cid', "A95*") == 1

replace `new_var' = 1 if match(`var_cid', "B26*") | match(`var_cid', "B06*") | match(`var_cid', "B05*") | match(`var_cid', "B16*") == 1
replace `new_var' = 1 if match(`var_cid', "B50*") | match(`var_cid', "B51*") | match(`var_cid', "B52*") | match(`var_cid', "B53*") | match(`var_cid', "B54*") == 1
replace `new_var' = 1 if match(`var_cid', "B77*") == 1

replace `new_var' = 1 if match(`var_cid', "D50*") == 1

replace `new_var' = 1 if match(`var_cid', "E10*") | match(`var_cid', "E11*") | match(`var_cid', "E12*") | match(`var_cid', "E13*") | match(`var_cid', "E14*") == 1

replace `new_var' = 1 if match(`var_cid', "E40*") | match(`var_cid', "E41*") | match(`var_cid', "E42*") | match(`var_cid', "E43*") | match(`var_cid', "E44*") | match(`var_cid', "E45*") | match(`var_cid', "E46*") == 1
replace `new_var' = 1 if match(`var_cid', "E5*") | match(`var_cid', "E60*") | match(`var_cid', "E61*") | match(`var_cid', "E62*") | match(`var_cid', "E63*") | match(`var_cid', "E64*") == 1
replace `new_var' = 1 if match(`var_cid', "E86*") == 1

replace `new_var' = 1 if match(`var_cid', "G000*") == 1
replace `new_var' = 1 if match(`var_cid', "G40*") | match(`var_cid', "G41*") == 1
replace `new_var' = 1 if match(`var_cid', "G45*") | match(`var_cid', "G46*") == 1

replace `new_var' = 1 if match(`var_cid', "H66*") == 1

replace `new_var' = 1 if match(`var_cid', "I00*") | match(`var_cid', "I01*") | match(`var_cid', "I02*") == 1
replace `new_var' = 1 if match(`var_cid', "I10*") | match(`var_cid', "I11*") | match(`var_cid', "I20*") | match(`var_cid', "I50*") == 1
replace `new_var' = 1 if match(`var_cid', "I63*") |  match(`var_cid', "I64*") | match(`var_cid', "I65*") | match(`var_cid', "I66*") | match(`var_cid', "I67*") | match(`var_cid', "I69*") == 1

replace `new_var' = 1 if match(`var_cid', "J00*") | match(`var_cid', "J01*") | match(`var_cid', "J02*") | match(`var_cid', "J03*") | match(`var_cid', "J06*") == 1
replace `new_var' = 1 if match(`var_cid', "J13*") | match(`var_cid', "J14*") | match(`var_cid', "J153*") | match(`var_cid', "J154*") | match(`var_cid', "J158*") | match(`var_cid', "J159*") | match(`var_cid', "J181*") == 1
replace `new_var' = 1 if match(`var_cid', "J20*") | match(`var_cid', "J21*") == 1
replace `new_var' = 1 if match(`var_cid', "J31*") | match(`var_cid', "J40*") | match(`var_cid', "J41*") | match(`var_cid', "J42*") | match(`var_cid', "J43*") | match(`var_cid', "J44*") | ///
							match(`var_cid', "J45*") | match(`var_cid', "J46*") | match(`var_cid', "J47*") == 1
replace `new_var' = 1 if match(`var_cid', "J81*") == 1

replace `new_var' = 1 if match(`var_cid', "K25*") | match(`var_cid', "K26*") | match(`var_cid', "K27*") | match(`var_cid', "K28*") | match(`var_cid', "K920*") | match(`var_cid', "K921*") | match(`var_cid', "K922*") == 1
replace `new_var' = 1 if match(`var_cid', "L01*") | match(`var_cid', "L02*") | match(`var_cid', "L03*") | match(`var_cid', "L04*") | match(`var_cid', "L08*") == 1

replace `new_var' = 1 if match(`var_cid', "N10*") | match(`var_cid', "N11*") | match(`var_cid', "N12*") | match(`var_cid', "N30*") | match(`var_cid', "N34*") | match(`var_cid', "N390*") == 1
replace `new_var' = 1 if match(`var_cid', "N70*") | match(`var_cid', "N71*") | match(`var_cid', "N72*") | match(`var_cid', "N73*") | match(`var_cid', "N75*") | match(`var_cid', "N76*") == 1

replace `new_var' = 1 if match(`var_cid', "O23*") == 1
replace `new_var' = 1 if match(`var_cid', "P350*") == 1

replace `new_var' = 0 if missing(`new_var')
			
	}
	
end
