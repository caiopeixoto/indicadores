
*** Cria Programa para classificar causas evitaveis de obitos

**** CID-10: http://apps.who.int/classifications/icd10/browse/2016/
**** Classificaçao baseada em Nolte & McKee (2003, 2008, 2011)
*** Nolte E, McKee CM. Measuring the health of nations: updating an earlier analysis. Health Aff (Millwood) 2008;27:58-71.
*** Nolte E, McKee M. Measuring the health of nations: analysis of mortality amenable to health care. Bmj 2003;327:1129.
*** Nolte E, McKee M. Variations in amenable mortality--trends in 16 high-income nations. Health Policy 2011;103:47-52.

**** Cria programa
capture program drop evitaveis
program define evitaveis
	
	args var_cid nova_var idade_var
	
	{

* Intestinal infections
gen byte `nova_var' = 1 if `idade_var' <= 414 & (match(`var_cid', "A0*")) == 1
* Tuberculosis
replace `nova_var' = 1 if `idade_var' <= 474 & (match(`var_cid', "A15*") | match(`var_cid', "A16*") | match(`var_cid', "A17*") | match(`var_cid', "A18*") | match(`var_cid', "A19*") | match(`var_cid', "B90*") ) == 1
* Other infections (diphteria, tetanus, poliomyelitis)
replace `nova_var' = 1 if `idade_var' <= 474 & (match(`var_cid', "A35*") | match(`var_cid', "A36*") | match(`var_cid', "A80*") )   == 1
* Whooping cough 
replace `nova_var' = 1 if (`idade_var' <= 414) & (match(`var_cid', "A37*")) == 1
* Septicaemia 
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "A40*") | match(`var_cid', "A41*") ) == 1
* Measles 
replace `nova_var' = 1 if (`idade_var' > 400) & (`idade_var' < 415) & (match(`var_cid', "B05*") ) == 1
* Malignant neoplasm of colon and rectum 
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "C18*") | match(`var_cid', "C19*") | match(`var_cid', "C20*") | match(`var_cid', "C21*") ) == 1
* Malignant neoplasm of skin
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "C44*") ) == 1
* Malignant neoplasm of breast
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "C50*") ) == 1
* Malignant neoplasm of uteri
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "C53*") ) == 1
* Malignant neoplasm of cervix uteri and body of uterus
replace `nova_var' = 1 if (`idade_var' <= 444) & (match(`var_cid', "C54*") | match(`var_cid', "C55*") ) == 1
* Malignant neoplasm of testis
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "C62*")) == 1
* Hodgkin's disease
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "C81*")) == 1
* Leukaemia 
replace `nova_var' = 1 if (`idade_var' <= 444) & (match(`var_cid', "C91*") | match(`var_cid', "C92*") | match(`var_cid', "C93*") | match(`var_cid', "C94*") | match(`var_cid', "C95*") ) == 1
* Diseases of the thyroid
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "E00*") | match(`var_cid', "E01*") | match(`var_cid', "E02*") | match(`var_cid', "E03*") | match(`var_cid', "E04*") | match(`var_cid', "E05*") | match(`var_cid', "E06*") | match(`var_cid', "E07*") ) == 1
* Diabetes mellitus
replace `nova_var' = 1 if (`idade_var' <= 449) & (match(`var_cid', "E10*") | match(`var_cid', "E11*") | match(`var_cid', "E12*") | match(`var_cid', "E13*") | match(`var_cid', "E14*") ) == 1
* Epilepsy 
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "G40*") | match(`var_cid', "G41*") ) == 1
* Chronic rheumatic heart disease
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "I05*") | match(`var_cid', "I06*") | match(`var_cid', "I07*") | match(`var_cid', "I08*") | match(`var_cid', "I09*") ) == 1
* Hypertensive disease
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "I10*") | match(`var_cid', "I11*") | match(`var_cid', "I12*") | match(`var_cid', "I13*") | match(`var_cid', "I15*") ) == 1
* Cerebrovascular disease
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "I60*") ) == 1
* All respiratory diseases (excluding pneumonia and influenza)	
replace `nova_var' = 1 if ((`idade_var' > 400) & (`idade_var' < 415)) & (match(`var_cid', "J0*") | match(`var_cid', "J20*") | match(`var_cid', "J30*") | match(`var_cid', "J40*") | match(`var_cid', "J50*") | match(`var_cid', "J60*") | match(`var_cid', "J70*") | match(`var_cid', "J80*") | match(`var_cid', "J90*") ) == 1
* Influenza
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "J10*") | match(`var_cid', "J11*") ) == 1
* Pneumonia 
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "J12*") | match(`var_cid', "J13*") | match(`var_cid', "J14*") | match(`var_cid', "J15*") | match(`var_cid', "J16*") | match(`var_cid', "J17*") | match(`var_cid', "J18*")) == 1
* Peptic ulcer 
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "K25*") | match(`var_cid', "K26*") | match(`var_cid', "K27*") ) == 1
* Appendicitis 
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "K35*") | match(`var_cid', "K36*") | match(`var_cid', "K37*") | match(`var_cid', "K38*") ) == 1
* Abdominal hernia 
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "K40*") | match(`var_cid', "K41*") | match(`var_cid', "K42*") | match(`var_cid', "K43*") | match(`var_cid', "K44*") | match(`var_cid', "K45*") | match(`var_cid', "K46*") ) == 1
* Cholelithiasis and cholecystitis
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "K80*") | match(`var_cid', "K81*") ) == 1
* Nephritis and nephrosis
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "N00*") | match(`var_cid', "N01*") | match(`var_cid', "N02*") | match(`var_cid', "N03*") | match(`var_cid', "N04*") | match(`var_cid', "N05*") | match(`var_cid', "N06*") | match(`var_cid', "N07*") | match(`var_cid', "N17*") | match(`var_cid', "N18*") | match(`var_cid', "N19*") | match(`var_cid', "N25*") | match(`var_cid', "N26*") | match(`var_cid', "N27*")) == 1
* Benign prostatic hyperplasia
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "N40*") ) == 1
* Maternal death
replace `nova_var' = 1 if match(`var_cid', "O*") == 1
* Congenital cardiovascular anomalies
replace `nova_var' = 1 if (`idade_var' <= 474) & (match(`var_cid', "Q20*") | match(`var_cid', "Q21*") | match(`var_cid', "Q22*") | match(`var_cid', "Q23*") | match(`var_cid', "Q24*") | match(`var_cid', "Q25*") | match(`var_cid', "Q26*") | match(`var_cid', "Q27*") | match(`var_cid', "Q28*") ) == 1
* Perinatal deaths, all causes, excluding stillbirths
replace `nova_var' = 1 if match(`var_cid', "A33*") | match(`var_cid', "P00*") | match(`var_cid', "P1*") | match(`var_cid', "P2*") | match(`var_cid', "P3*") | match(`var_cid', "P4*") | match(`var_cid', "P5*") | match(`var_cid', "P6*") | match(`var_cid', "P7*") | match(`var_cid', "P8*") | match(`var_cid', "P90*") | match(`var_cid', "P91*") | match(`var_cid', "P92*") | match(`var_cid', "P93*") | match(`var_cid', "P94*") | match(`var_cid', "P95*") | match(`var_cid', "P96*") == 1
* Misadventures to patients during surgical and medical care
replace `nova_var' = 1 if match(`var_cid', "Y6*") | match(`var_cid', "Y83*")|match(`var_cid', "Y84*") == 1
* Ischaemic heart disease
replace `nova_var' = 1 if (`idade_var' <= 474) & match(`var_cid', "I20*") | match(`var_cid', "I21*") | match(`var_cid', "I22*")| match(`var_cid', "I23*")|match(`var_cid', "I24*")| match(`var_cid', "I25*") == 1

replace `nova_var' = 0 if missing(`nova_var')

	}
	
end