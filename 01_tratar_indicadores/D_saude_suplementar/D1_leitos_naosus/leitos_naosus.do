* IEPS DATA

* Esse codigo trata os dados de recursos

														

********************************************************************************
* 0. Config
********************************************************************************

**** Define lista de anos e meses a serem tratados
global meses = `"  "1012" "1112" "1212" "1312" "1412" "1512" "1612" "1712" "1812" "1912" "2012" "'


**** Setar diretorio
cd "Z:/"


****  Input
global path_lt  = "Master/Datasus/raw/CNES/LT/dta/"
global path_pop = "Projects/IEPS Data/Indicadores/Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta"
global path_reg_saude =  "Projects/IEPS Data/Indicadores/Data/auxiliary/Regioes de Saude.dta"
global path_macro_saude =  "Projects/IEPS Data/Indicadores/Data/auxiliary/Macrorregioes de Saude.dta"


****  Output
global sf_mun_temp_1 = "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_temp.dta"
global sf_mun_temp_2 = "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_temp.dta"
global leitos_nsus_mun = "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_mun.dta"
global leitos_nsus_reg = "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_reg.dta"
global leitos_nsus_macro ="Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_macro.dta" 
global leitos_nsus_uf = "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_uf.dta" 
global leitos_nsus_br = "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D1_leitos_naosus/leitos_nsus_br.dta" 


********************************************************************************
* 1. Leitos: Tratar Dados - Abre e Prepara Dados
********************************************************************************
clear
foreach mes of global meses {
local files: dir "$path_lt" files "*`mes'.dta"
	foreach f of local files {
	di "`f'"
	append using "${path_lt}`f'", keep(CODUFMUN QT_SUS QT_NSUS TP_LEITO CODLEITO COMPETEN)
	}	
}

**** Tira capslock dos nomes das variaveis
rename *, lower

**** Renomeia e transforma codigo de municipio em numerica
destring codufmun, replace
rename codufmun codmun

**** Imputa o código de Brasilia a municípios que são do DF mas com outro codigo de municipio
replace codmun = 530010 if floor(codmun/10000) == 53

**** Gera variavel de ano
gen ano = real(substr(competen,1,4))
drop competen

**** Transforma variaveis em numericas
destring codleito, replace
destring tp_leito, replace

**** Descarta leitos psiquiatricos
*drop if tp_leito==5
drop if codleito == 47

**** Gera variavel de Leitos de UTI nao-SUS
gen n_leitouti_nsus = qt_nsus if inrange(codleito,61,63) | ///
inrange(codleito,74,83) | inrange(codleito,85,86)

replace n_leitouti_nsus = 0 if n_leitouti_nsus==.

**** Gera variavel de Leitos nao-SUS
gen n_leito_nsus=qt_nsus

********************************************************************************
* 2 Junta bases
********************************************************************************

**** Altera formato de variavel de codigo municipal
destring codmun, replace

**** Agrupa no nivel do municipio e ano
collapse (sum) n_leitouti_nsus n_leito_nsus, by(codmun ano)

**** Salva primeira base municipal intermediaria
save "$sf_mun_temp_1", replace

**** Junta com base populacional
use "$path_pop", clear
keep ano codmun pop
merge 1:m ano codmun using "$sf_mun_temp_1"
keep if _merge == 3 | _merge == 1
drop _merge

**** Transforma missings em zero
replace n_leito_nsus = 0 if missing(n_leito_nsus)
replace n_leitouti_nsus = 0 if missing(n_leitouti_nsus)

**** Faz o merge com dados de unidades geograficas
rename codmun ibge
merge m:1 ibge using "$path_reg_saude", keepusing(regiao) nogen
merge m:1 ibge using "$path_macro_saude", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"


**** Calcular indicadores de taxas
gen tx_leitouti_nsus = 100000*(n_leitouti_nsus/pop)
gen tx_leito_nsus = 100000*(n_leito_nsus/pop)

**** Salva segunda base municipal intermediaria
compress
save "$sf_mun_temp_2", replace

********************************************************************************
* 3. Agrega para niveis geograficos
********************************************************************************

//---------------------------------------------------------------
**** 3.1. Agrega para municipio
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

**** Mantem somente variaveis necessarias
keep ano codmun n_leitouti_nsus n_leito_nsus tx_leitouti_nsus tx_leito_nsus

save "$leitos_nsus_mun", replace


//---------------------------------------------------------------
**** 3.2. Agrega para regiao de saude
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

**** Agrupa no nivel da regiao e ano
collapse (sum) n_leitouti_nsus n_leito_nsus pop, by(regiao ano)

**** Calcular indicadores de taxas
gen tx_leitouti_nsus = 100000*(n_leitouti_nsus/pop)
gen tx_leito_nsus = 100000*(n_leito_nsus/pop)

**** Selecionar colunas para salvar
keep ano regiao n_leitouti_nsus n_leito_nsus tx_leitouti_nsus tx_leito_nsus

**** Salvar
save "$leitos_nsus_reg", replace


//---------------------------------------------------------------
**** 3.3. Agrega para macrorregiao de saude
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

**** Agrupa no nivel da regiao e ano
collapse (sum) n_leitouti_nsus n_leito_nsus pop, by(macrorregiao ano)

**** Calcular indicadores de taxas
gen tx_leitouti_nsus = 100000*(n_leitouti_nsus/pop)
gen tx_leito_nsus = 100000*(n_leito_nsus/pop)

**** Selecionar colunas para salvar
keep ano macrorregiao n_leitouti_nsus n_leito_nsus tx_leitouti_nsus tx_leito_nsus

**** Salvar
save "$leitos_nsus_macro", replace


//---------------------------------------------------------------
**** 3.4. Agrega para UF
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

**** Agrupa no nivel da regiao e ano
collapse (sum) n_leitouti_nsus n_leito_nsus pop*, by(id_estado ano)

**** Calcular indicadores de taxas
gen tx_leitouti_nsus = 100000*(n_leitouti_nsus/pop)
gen tx_leito_nsus = 100000*(n_leito_nsus/pop)

**** Selecionar colunas para salvar
keep ano id_estado n_leitouti_nsus n_leito_nsus tx_leitouti_nsus tx_leito_nsus

**** Salvar
save "$leitos_nsus_uf", replace


//---------------------------------------------------------------
**** 3.5. Agrega para Brasil
//---------------------------------------------------------------
use "$sf_mun_temp_2", clear

**** Agrupa no nivel da regiao e ano
collapse (sum) n_leitouti_nsus n_leito_nsus pop*, by(ano)

**** Calcular indicadores de taxas
gen tx_leitouti_nsus = 100000*(n_leitouti_nsus/pop)
gen tx_leito_nsus = 100000*(n_leito_nsus/pop)

**** Selecionar colunas para salvar
keep ano n_leitouti_nsus n_leito_nsus tx_leitouti_nsus tx_leito_nsus

**** Salvar
save "$leitos_nsus_br", replace

