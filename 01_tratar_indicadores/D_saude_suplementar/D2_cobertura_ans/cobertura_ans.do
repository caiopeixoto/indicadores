
* IEPS DATA

* Esse codigo trata os dados de cobertura ANS
														

********************************************************************************
* 0. Config
********************************************************************************
clear


**** Seta diretorio
cd "Z:\"

**** Input 
global path_cobertura   "Projects/IEPS Data/Indicadores/Data/01_input_data/D_saude_suplementar/D2_cobertura_ans/ans_10-19.csv" 
global path_cobertura20 "Projects/IEPS Data/Indicadores/Data/01_input_data/D_saude_suplementar/D2_cobertura_ans/ans_20.csv" 
global path_pop_idade   "Projects/IEPS Data/Indicadores/Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta" // dados de população por idade
global path_reg_saude   "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Regioes_de_Saude.dta"
global path_macro_saude "Projects/IEPS Data/Indicadores/Data/02_temp_data/0_auxiliar/Macrorregioes de Saude.dta"


**** Output 
global ans_temp   "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_temp.dta" 
global ans_temp2  "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_temp2.dta" 
global ans_mun    "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_mun.dta" 
global ans_reg    "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_reg.dta" 
global ans_macro  "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_macro.dta" 
global ans_uf     "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_uf.dta" 
global ans_br     "Projects/IEPS Data/Indicadores/Data/02_temp_data/D_saude_suplementar/D2_cobertura_ans/ans_br.dta" 



********************************************************************************
* 1.1 Tratar Dados - Abre e Prepara Dados
********************************************************************************

**** Carregar dados 2010 - 2019
import delimited "$path_cobertura", varnames(4) clear

**** Gerar codigo municipal e descartar totais
gen codmun = substr(municpio,1,6)
drop if municpio=="Total" 
destring codmun, replace
drop municpio
order codmun

**** Renomear variaveis 
rename jun jun19
rename mar mar19
rename v8 jun18
rename v9 mar18
rename v12 jun17
rename v13 mar17
rename v16 jun16
rename v17 mar16
rename v20 jun15
rename v23 mar14
rename v26 jun13
rename v27 mar13
rename v30 jun12
rename v31 mar12 
rename v34 jun11
rename v35 mar11
rename v38 jun10
rename v39 mar10

**** Adicionar prefixo para variaveis (exceto codmun) para facilitar Reshape 
quietly ds codmun , not
rename (`r(varlist)') (benef=)

**** Fazer reshape dos dados do tipo long (cria coluna de ano)
reshape long benefdez benefset benefjun benefmar, i(codmun) j(ano) string

**** Adiciona digitos ao ano
replace ano = "20" + ano
destring ano, replace

**** Manter apenas dezembro
drop benefmar benefjun benefset

**** Renomear variavel
rename benefdez benef

**** Descartar codigos municipais deconhecidos
drop if codmun==0 | codmun>530010

* Salvar temp	
compress
save "$ans_temp", replace

**** Carregar dados 2020
import delimited "$path_cobertura20", varnames(1) clear

**** Gerar codigo municipal e descartar totais
gen codmun = substr(municãpio,1,6)
drop if municãpio=="Total" 
drop if municãpio=="&" 
destring codmun, replace
drop municãpio
order codmun


**** Adicionar prefixo para variaveis (exceto codmun) para facilitar Reshape 
quietly ds codmun , not
rename (`r(varlist)') (benef=)

**** Fazer reshape dos dados do tipo long (cria coluna de ano)
reshape long benefdez benefset benefjun benefmar, i(codmun) j(ano) string

**** Adiciona digitos ao ano
replace ano = "20" + ano
destring ano, replace

**** Manter apenas dezembro
drop benefmar benefjun benefset

**** Renomear variavel
rename benefdez benef


**** Imputa o código de Brasilia a municípios que são do DF mas com código ignorado
replace codmun = 530010 if floor(codmun/10000) == 53

**** Descartar codigos municipais deconhecidos
drop if codmun==0 | codmun>530010 


**** Empilha com a base de 2010-2019 
append using "$ans_temp"



********************************************************************************
* 1.2 Junta bases e agrega para municipio/ano
********************************************************************************

**** Faz o merge com dados de unidades geograficas e de população por idade 
rename codmun ibge
merge m:1 ibge using "$path_reg_saude", keepusing(regiao) nogen
merge m:1 ibge using "$path_macro_saude", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"
merge m:1 codmun ano using "${path_pop_idade}", 
keep if _merge==3
drop _merge

* Salvar temp	
compress
save "$ans_temp2", replace

**** Criar variavel de % de cobertura 
gen cob_priv = 100*(benef/pop)
**** Coberturas > 100% devem ser consideradas como 100% (15 obs)
replace cob_priv = 100 if cob_priv >= 100
**** Descartar variaveis
keep codmun ano cob_priv
**** Salvar
save "$ans_mun", replace



********************************************************************************
* 1.3 Agrega para demais niveis geograficos
********************************************************************************

**** Agrega para regiao de saude
use "$ans_temp2", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) benef* pop*, by(regiao ano)
**** Criar variavel de % de cobertura 
gen cob_priv = 100*(benef/pop)
**** Coberturas > 100% devem ser consideradas como 100% (15 obs)
replace cob_priv = 100 if cob_priv >= 100
**** Descartar variaveis
keep regiao ano cob_priv
**** Salvar
save "$ans_reg", replace



**** Agrega para macrorregiao de saude
use "$ans_temp2", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) benef* pop*, by(macrorregiao ano)
**** Criar variavel de % de cobertura 
gen cob_priv = 100*(benef/pop)
**** Coberturas > 100% devem ser consideradas como 100% (15 obs)
replace cob_priv = 100 if cob_priv >= 100
**** Descartar variaveis
keep macrorregiao ano cob_priv
**** Salvar
save "$ans_macro", replace



**** Agrega para UF
use "$ans_temp2", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) benef* pop*, by(id_estado ano)
**** Criar variavel de % de cobertura 
gen cob_priv = 100*(benef/pop)
**** Coberturas > 100% devem ser consideradas como 100% (15 obs)
replace cob_priv = 100 if cob_priv >= 100
**** Descartar variaveis
keep id_estado ano cob_priv
**** Salvar
save "$ans_uf", replace



**** Agrega para o Brasil
use "$ans_temp2", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) benef* pop*, by(ano)
**** Criar variavel de % de cobertura 
gen cob_priv = 100*(benef/pop)
**** Coberturas > 100% devem ser consideradas como 100% (15 obs)
replace cob_priv = 100 if cob_priv >= 100
**** Descartar variaveis
keep ano cob_priv
**** Salvar
save "$ans_br", replace


