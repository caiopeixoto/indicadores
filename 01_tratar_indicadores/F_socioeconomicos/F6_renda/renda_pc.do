* IEPS DATA

* Esse codigo trata dados de renda domiciliar media per capita 

														

********************************************************************************
* 0. Config
********************************************************************************

ssc install asgen

**** Define diretorio
cd "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/F_socioeconomicos"

**** Input 
global path_renda = "Z:/Projects/IEPS Data/Indicadores/Data/01_input_data/F_socioeconomicos/F6_renda/renda_domiciliar_pc.xlsx"
global path_reg_saude = "Z:/Projects/IEPS Data/Indicadores/Data/auxiliary/Regioes de Saude.dta"
global path_macro_saude = "Z:/Projects/IEPS Data/Indicadores/Data/auxiliary/Macrorregioes de Saude.dta"
global path_pop "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta"

**** Output 
global renda_temp = "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/F_socioeconomicos/F6_renda/renda_temp.dta"
global renda_mun = "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/F_socioeconomicos/F6_renda/renda_mun.dta"
global renda_reg = "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/F_socioeconomicos/F6_renda/renda_reg.dta"
global renda_macro = "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/F_socioeconomicos/F6_renda/renda_macro.dta"
global renda_uf = "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/F_socioeconomicos/F6_renda/renda_uf.dta"
global renda_br = "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/F_socioeconomicos/F6_renda/renda_br.dta"


********************************************************************************
* 1. Tratar Dados - Abre e Prepara Dados
********************************************************************************
clear
import excel using "$path_renda", cellrange(A6:C5571) firstrow

**** Gera codigo de municipio de seis digitos e descarta variaveis
gen codmun = substr(Cód,1,6)
destring codmun, replace
drop Cód
drop Município

rename C renda_dom_pc 
gen ano = 2010

**** Faz o merge com dados de unidades geograficas e de população por idade 
rename codmun ibge
merge m:1 ibge using "$path_reg_saude", keepusing(regiao) nogen
merge m:1 ibge using "$path_macro_saude", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"
merge m:1 codmun ano using "${path_pop}", keepusing(pop) 
keep if _merge==3
drop _merge

**** Expandir para criar painel 2010-2020 (repete obs de 2010, unico ano disponivel)
drop ano
expand 11
by codmun, sort : gen n=_n
gen ano=n
replace ano=2010 if ano==1
replace ano=2011 if ano==2
replace ano=2012 if ano==3
replace ano=2013 if ano==4
replace ano=2014 if ano==5
replace ano=2015 if ano==6
replace ano=2016 if ano==7
replace ano=2017 if ano==8
replace ano=2018 if ano==9
replace ano=2019 if ano==10
replace ano=2020 if ano==11
drop n    // remove coluna temporario


**** Salva base municipal intermediaria
save "$renda_temp", replace

********************************************************************************
* 2. Agrega para niveis geograficos
********************************************************************************

//---------------------------------------------------------------
**** 2.1. Agrega para municipio
//---------------------------------------------------------------
use "$renda_temp", clear

**** Mantem somente variaveis necessarias
keep codmun ano renda_dom_pc

save "$renda_mun", replace

//---------------------------------------------------------------
**** 2.2. Agrega para regiao de saude
//---------------------------------------------------------------

use "$renda_temp", clear

collapse (mean) renda_dom_pc [w=pop] , by(regiao ano) 

keep regiao ano renda_dom_pc

save "$renda_reg", replace

//---------------------------------------------------------------
**** 2.3. Agrega para macrorregiao de saude
//---------------------------------------------------------------

use "$renda_temp", clear

collapse (mean) renda_dom_pc [w=pop] , by(macrorregiao ano) 

keep macrorregiao ano renda_dom_pc

save "$renda_macro", replace

//---------------------------------------------------------------
**** 2.4. Agrega para UF
//---------------------------------------------------------------

use "$renda_temp", clear

collapse (mean) renda_dom_pc [w=pop] , by(id_estado ano) 

keep id_estado ano renda_dom_pc

save "$renda_uf", replace


//---------------------------------------------------------------
**** 2.5. Agrega para BR
//---------------------------------------------------------------

use "$renda_temp", clear

collapse (mean) renda_dom_pc [w=pop] , by(ano) 

keep ano renda_dom_pc

save "$renda_br", replace