* IEPS DATA

* Esse codigo trata os dados de PIB municipal

														

********************************************************************************
* 0. Config
********************************************************************************
clear

**** Seta diretorio
cd "Z:/Projects/IEPS Data/Indicadores/"

**** Input
global path_pib         "Data/01_input_data/F_socioeconomicos/F5_pib/pib_mun_2010_2019.xlsx"
global path_deflator    "Data/01_input_data/F_socioeconomicos/F5_pib/deflator_pib.xlsx"
global path_pop   		"Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta"
global path_reg_saude   "Data/02_temp_data/0_auxiliar/Regioes_de_Saude.dta"
global path_macro_saude "Data/02_temp_data/0_auxiliar/Macrorregioes de Saude.dta"

**** Output 
global pib_temp_1    = "Data/02_temp_data/F_socioeconomicos/F5_pib/pib_temp_1.dta"
global pib_temp_2    = "Data/02_temp_data/F_socioeconomicos/F5_pib/pib_temp_2.dta"
global pib_def       = "Data/02_temp_data/F_socioeconomicos/F5_pib/pib_def.dta"
global pib_def_mun   = "Data/02_temp_data/F_socioeconomicos/F5_pib/pib_def_mun.dta"
global pib_def_reg   = "Data/02_temp_data/F_socioeconomicos/F5_pib/pib_def_reg.dta"
global pib_def_macro = "Data/02_temp_data/F_socioeconomicos/F5_pib/pib_def_macro.dta"
global pib_def_uf    = "Data/02_temp_data/F_socioeconomicos/F5_pib/pib_def_uf.dta"
global pib_def_br    = "Data/02_temp_data/F_socioeconomicos/F5_pib/pib_def_br.dta"



********************************************************************************
* 1.1 Tratar Dados - Abre e Prepara Dados
********************************************************************************
**** Importa dados de PIB municipal
import excel using "$path_pib", cellrange(A4:D55703) clear

**** Renomeia variaveis
rename A cod
rename C ano
rename D pib_corr

**** Preenche codigo de municipio
replace cod=cod[_n-1] if cod==""

**** Gera codigo de municipio de seis digitos
destring cod, replace
gen codmun = floor(cod/10)
drop cod
drop B

**** Transforma variaveis em numericas
replace pib_corr = "" if pib_corr == "..." 
destring pib_corr, replace
destring ano, replace

**** Salva arquivo temporario
save "$pib_temp_1", replace

**** Importa base de deflator do PIB
import excel using "$path_deflator", firstrow clear

**** Mantem somente variaveis necessarias
keep ano def
destring ano, replace

**** Junta com base de pib
merge 1:m ano using "$pib_temp_1", nogen

**** Gera PIB constante (valores de 2018)
gen pib_cte = pib_corr*def

**** Descarta deflator e PIB corrente
drop def
drop pib_corr


********************************************************************************
* 1.2 Junta bases e agrega para municipio/ano
********************************************************************************


**** Faz o merge com dados de unidades geograficas e de população por idade 
rename codmun ibge
merge m:1 ibge using "$path_reg_saude", keepusing(regiao) nogen
merge m:1 ibge using "$path_macro_saude", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"
merge m:1 codmun ano using "${path_pop}", 
keep if _merge==3
drop _merge

**** Gera base temporaria
save "$pib_temp_2", replace

**** Gera valores per capita
gen pib_cte_pc = pib_cte/pop

**** Inclui legendas para as variaveis
label var ano "Ano"
la var codmun "Codigo do Municipio"
label var pib_cte_pc "Produto Interno Bruto per capita (valores de 2019)"

**** Ordena variaveis
order ano codmun 

**** Seleciona variaveis
keep ano codmun pib_cte_pc

**** Salva
save "$pib_def_mun", replace



********************************************************************************
* 1.3 Agrega para demais niveis geograficos
********************************************************************************

**** Agrega para regiao de saude
use "$pib_temp_2", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) pib_cte* pop*, by(regiao ano)
**** Gera valores per capita
gen pib_cte_pc = pib_cte/pop
**** Inclui legendas para as variaveis
label var ano "Ano"
la var regiao "Codigo da Regiao de Saude"
label var pib_cte_pc "Produto Interno Bruto per capita (valores de 2019)"
**** Ordena variaveis
order ano regiao 
**** Seleciona variaveis
keep ano regiao pib_cte_pc
**** Salva
save "$pib_def_reg", replace



**** Agrega para macrorregiao de saude
use "$pib_temp_2", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) pib_cte* pop*, by(macrorregiao ano)
**** Gera valores per capita
gen pib_cte_pc = pib_cte/pop
**** Inclui legendas para as variaveis
label var ano "Ano"
la var macrorregiao "Codigo da Macrorregiao de Saude"
label var pib_cte_pc "Produto Interno Bruto per capita (valores de 2019)"
**** Ordena variaveis
order ano macrorregiao
**** Seleciona variaveis
keep ano macrorregiao pib_cte_pc
**** Salva
save "$pib_def_macro", replace



**** Agrega para UF
use "$pib_temp_2", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) pib_cte* pop*, by(id_estado ano)
**** Gera valores per capita
gen pib_cte_pc = pib_cte/pop
**** Inclui legendas para as variaveis
label var ano "Ano"
la var id_estado "Codigo da UF"
label var pib_cte_pc "Produto Interno Bruto per capita (valores de 2019)"
**** Ordena variaveis
order ano id_estado 
**** Seleciona variaveis
keep ano id_estado pib_cte_pc
**** Salva
save "$pib_def_uf", replace



**** Agrega para o Brasil
use "$pib_temp_2", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) pib_cte* pop*, by(ano)
**** Gera valores per capita
gen pib_cte_pc = pib_cte/pop
**** Inclui legendas para as variaveis
label var ano "Ano"
label var pib_cte_pc "Produto Interno Bruto per capita (valores de 2019)"
**** Ordena variaveis
order ano  
**** Seleciona variaveis
keep ano pib_cte_pc
**** Salva
save "$pib_def_br", replace

