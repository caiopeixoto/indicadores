
* IEPS DATA

* Esse codigo trata dados de idh municipal

														

********************************************************************************
* 0. Config
********************************************************************************
clear


**** Define diretorio
cd "Z:/Projects/IEPS Data/Indicadores"


**** Input 
global path_idhm        "Data/01_input_data\F_socioeconomicos\F3_idhm\Atlas 2013_municipal, estadual e Brasil.xlsx"
global path_pop_idade   "Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta" // dados de população por idade
global path_reg_saude   "Data/02_temp_data/0_auxiliar/Regioes_de_Saude.dta"
global path_macro_saude "Data/02_temp_data/0_auxiliar/Macrorregioes de Saude.dta"

**** Output 
global idhm_temp  "Data\02_temp_data\F_socioeconomicos\F3_idhm\idhm_temp.dta" 
global idhm_mun   "Data\02_temp_data\F_socioeconomicos\F3_idhm\idhm_mun.dta" 

********************************************************************************
* 1.1 Tratar Dados - Abre e Prepara Dados
********************************************************************************

**** Abrir dados
import excel  "$path_idhm", sheet("MUN 91-00-10") firstrow

**** Selecionar ano do Censo (mais recente)
keep if ANO == 2010

**** Renomear variaveis
rename ANO ano
rename UF uf
rename Codmun7 codmun
rename ESPVIDA exp_vida
rename IDHM idhm
rename IDHM_E idhm_educ
rename IDHM_L idhm_long
rename IDHM_R idhm_renda

**** Selecionar variaveis para salvar
keep ano codmun idhm idhm_educ idhm_long idhm_renda exp_vida



********************************************************************************
* 1.2 Junta bases e salva para municipio
********************************************************************************

tostring codmun, replace
replace codmun = substr(codmun,1,6)
destring codmun, replace

**** Faz o merge com dados de unidades geograficas e de população por idade 
rename codmun ibge
merge m:1 ibge using "$path_reg_saude", keepusing(regiao) nogen
merge m:1 ibge using "$path_macro_saude", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"
merge m:1 codmun ano using "${path_pop_idade}", keepusing(pop) 
keep if _merge==3
drop _merge

**** Expandir para criar painel 2010-2020 (repete obs de 2010, unico ano disponivel)
drop ano // exclui antiga coluna de ano
expand 11
by codmun, sort : gen n=_n
gen ano=n
replace ano=2010 if ano==1
replace ano=2011 if ano==2
replace ano=2012 if ano==3
replace ano=2013 if ano==4
replace ano=2014 if ano==5
replace ano=2015 if ano==6
replace ano=2016 if ano==7
replace ano=2017 if ano==8
replace ano=2018 if ano==9
replace ano=2019 if ano==10
replace ano=2020 if ano==11
drop n    // remove coluna temporario


* Salvar temp	
compress
save "$idhm_temp", replace

**** Selecionar variaveis para salvar
keep ano codmun idhm idhm_educ idhm_long idhm_renda exp_vida
* Salvar mun	
save "$idhm_mun", replace


