
* IEPS DATA

* Esse codigo agrega dados de idh municipal no nivel da macrorregiao de saude

														

********************************************************************************
* 0. Config
********************************************************************************
clear


**** Define diretorio
cd "Z:/Projects/IEPS Data/Indicadores"


**** Input 
global path_idhm        "Data/01_input_data\F_socioeconomicos\F3_idhm\Atlas 2013_municipal, estadual e Brasil.xlsx"
global path_pop_idade   "Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta" // dados de população por idade
global path_reg_saude   "Data/02_temp_data/0_auxiliar/Regioes_de_Saude.dta"
global path_macro_saude "Data/02_temp_data/0_auxiliar/Macrorregioes de Saude.dta"

**** Output 
global idhm_macro "Data\02_temp_data\F_socioeconomicos\F3_idhm\idhm_macro.dta" 



********************************************************************************
* 1.1 Tratar Dados - Abre e Prepara Dados
********************************************************************************

**** Abrir dados
import excel  "$path_idhm", sheet("MUN 91-00-10") firstrow

**** Selecionar ano do Censo (mais recente)
keep if ANO == 2010

**** Renomear variaveis
rename ANO ano
rename UF uf
rename Codmun7 codmun
rename ESPVIDA exp_vida
rename *, lower

**** Selecionar variaveis para salvar
keep ano codmun t_freq5a6 t_fund11a13 t_fund15a17 t_fund18m t_med18a20 pesotot peso5 peso6 peso1113 peso1517 peso18 peso1820 exp_vida rdpc



********************************************************************************
* 1.2 Junta bases e trata variaveis
********************************************************************************

tostring codmun, replace
replace codmun = substr(codmun,1,6)
destring codmun, replace

**** Faz o merge com dados de unidades geograficas e de população por idade 
rename codmun ibge
merge m:1 ibge using "$path_reg_saude", keepusing(regiao) nogen
merge m:1 ibge using "$path_macro_saude", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"
merge m:1 codmun ano using "${path_pop_idade}", keepusing(pop) 
keep if _merge==3
drop _merge

* cria variavel nova (sum pop 5 e 6 anos)
gen peso56 = peso5+peso6
drop peso5 peso6

* Agrega populacao por faixas etarias/regiao
bysort macrorregiao: egen peso56_macro   = sum(peso56)
bysort macrorregiao: egen peso1113_macro = sum(peso1113)
bysort macrorregiao: egen peso1517_macro = sum(peso1517)
bysort macrorregiao: egen peso1820_macro = sum(peso1820)
bysort macrorregiao: egen peso18_macro   = sum(peso18)
bysort macrorregiao: egen pesotot_macro  = sum(pesotot)


* Cria pct de faixas de populacao
gen pct_peso56   = peso56/peso56_macro
gen pct_peso1113 = peso1113/peso1113_macro
gen pct_peso1517 = peso1517/peso1517_macro
gen pct_peso1820 = peso1820/peso1820_macro
gen pct_peso18   = peso18/peso18_macro
gen pct_pesotot  = pesotot/pesotot_macro

* Dividir por 100 variaveis de frequencia escolar
replace t_freq5a6   = t_freq5a6/100
replace t_fund11a13 = t_fund11a13/100
replace t_fund15a17 = t_fund15a17/100
replace t_fund18m   = t_fund18m/100
replace t_med18a20  = t_med18a20/100


********************************************************************************
* 1.3 Constroi sub-indice para IDHM EDUC
********************************************************************************

* Cria subindices de Educacao

* subindice escolaridade
gen i_escolaridade_aux = t_fund18m*pct_peso18

* subindice frequencia escolar
gen i_freq_pop_56_aux                = t_freq5a6*pct_peso56
gen i_freq_pop_1113_aux              = t_fund11a13*pct_peso1113
gen i_freq_pop_1517_aux              = t_fund15a17*pct_peso1517
gen i_freq_pop_1820_aux              = t_med18a20*pct_peso1820


********************************************************************************
* 1.4 Constroi sub-indice para IDHM LONG + Expectativa de Vida
********************************************************************************
gen exp_vida_aux = exp_vida*pct_pesotot
gen idhm_long_aux = ((exp_vida-25)/(85-25))*pct_pesotot


********************************************************************************
* 1.5 Constroi sub-indice para IDHM RENDA
********************************************************************************
gen idhm_renda_aux              = ((ln(rdpc)-ln(8))/(ln(4033)-ln(8)))*pct_pesotot


********************************************************************************
* 1.6 Constroi sub-indice para IDHM 
********************************************************************************

* Colapsa variaveis ao nivel da macrorregiao de saude
collapse (sum) i_escolaridade_aux i_freq_pop_56_aux i_freq_pop_1113_aux i_freq_pop_1517_aux i_freq_pop_1820_aux idhm_renda_aux exp_vida_aux idhm_long_aux, by(macrorregiao)

* Finaliza IDHM-E 
gen i_freq_prop  = (i_freq_pop_56_aux + i_freq_pop_1113_aux + i_freq_pop_1517_aux + i_freq_pop_1820_aux)/4
gen idhm_educ  = ((i_escolaridade_aux^(1/3))*((i_freq_prop^(2/3))))

* dropar
drop i_escolaridade_aux i_freq_pop_56_aux i_freq_pop_56 i_freq_pop_1113_aux i_freq_pop_1113 i_freq_pop_1517_aux i_freq_pop_1517 i_freq_pop_1820_aux i_freq_pop_1820_aux

* renomear variaveis
rename idhm_renda_aux idhm_renda
rename idhm_long_aux idhm_long
rename exp_vida_aux exp_vida

* Constroi IDHM
gen idhm = (idhm_educ^(1/3))*(idhm_long^(1/3))*(idhm_renda^(1/3))

* keep
keep macrorregiao idhm_educ idhm_long idhm_renda idhm exp_vida

**** Expandir para criar painel 2010-2020 (repete obs de 2010, unico ano disponivel)
expand 11
by macrorregiao, sort : gen n=_n
gen ano=n
replace ano=2010 if ano==1
replace ano=2011 if ano==2
replace ano=2012 if ano==3
replace ano=2013 if ano==4
replace ano=2014 if ano==5
replace ano=2015 if ano==6
replace ano=2016 if ano==7
replace ano=2017 if ano==8
replace ano=2018 if ano==9
replace ano=2019 if ano==10
replace ano=2020 if ano==11
drop n    // remove coluna temporario


* Salvar macro
save "$idhm_macro", replace