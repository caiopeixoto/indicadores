
* IEPS DATA

* Esse codigo trata dados de idh estadual

														

********************************************************************************
* 0. Config
********************************************************************************
clear


**** Seta diretorio
cd "Z:\Projects\IEPS Data\Indicadores\Data\" // diretório com caminho para servidor


**** Input 
global path_idhm "01_input_data\F_socioeconomicos\F3_idhm\Atlas 2013_municipal, estadual e Brasil.xlsx"

**** Output 
global idhm_uf "02_temp_data\F_socioeconomicos\F3_idhm\idhm_uf.dta" 
global idhm_br "02_temp_data\F_socioeconomicos\F3_idhm\idhm_br.dta" 



********************************************************************************
* 1. Tratar Dados - Abre e Prepara Dados
********************************************************************************
**** 1.1. Dados Estaduais
**** Abrir dados
import excel  "$path_idhm", sheet("UF 91-00-10") firstrow

**** Selecionar ano do Censo (mais recente)
keep if ANO == 2010

**** Renomear variaveis
rename ANO ano
rename UF id_estado
rename ESPVIDA exp_vida
rename IDHM idhm
rename IDHM_E idhm_educ
rename IDHM_L idhm_long
rename IDHM_R idhm_renda


**** Expandir para criar painel 2010-2019 (repete obs de 2010, unico ano disponivel)
drop ano // exclui antiga coluna de ano
expand 11
by id_estado, sort : gen n=_n
gen ano=n
replace ano=2010 if ano==1
replace ano=2011 if ano==2
replace ano=2012 if ano==3
replace ano=2013 if ano==4
replace ano=2014 if ano==5
replace ano=2015 if ano==6
replace ano=2016 if ano==7
replace ano=2017 if ano==8
replace ano=2018 if ano==9
replace ano=2019 if ano==10
replace ano=2020 if ano==11
drop n    // remove coluna temporario

**** Selecionar variaveis para salvar
keep ano id_estado idhm idhm_educ idhm_long idhm_renda exp_vida

**** Salvar
save  "$idhm_uf", replace

**** 1.2. Dados NACIONAIS
**** Abrir dados
clear
import excel  "$path_idhm", sheet("BR 91-00-10") firstrow

**** Selecionar ano do Censo (mais recente)
keep if ANO == 2010

drop A

**** Renomear variaveis
rename ANO ano
rename ESPVIDA exp_vida
rename IDHM idhm
rename IDHM_E idhm_educ
rename IDHM_L idhm_long
rename IDHM_R idhm_renda

**** Expandir para criar painel 2010-2020 (repete obs de 2010, unico ano disponivel)
drop ano
expand 11
gen ano=_n
replace ano=2010 if ano==1
replace ano=2011 if ano==2
replace ano=2012 if ano==3
replace ano=2013 if ano==4
replace ano=2014 if ano==5
replace ano=2015 if ano==6
replace ano=2016 if ano==7
replace ano=2017 if ano==8
replace ano=2018 if ano==9
replace ano=2019 if ano==10
replace ano=2020 if ano==11
 // remove coluna temporario

**** Selecionar variaveis para salvar
keep ano idhm idhm_educ idhm_long idhm_renda exp_vida

**** Salvar
save  "$idhm_br", replace
