* IEPS DATA

* Esse codigo trata os dados do Censo
* referentes ao acesso a saneamento basico e populacao residente no meio rural

**** Fontes
* Rural x Urban population: https://sidra.ibge.gov.br/Tabela/1378
* Sanitation: https://sidra.ibge.gov.br/tabela/3216
														

********************************************************************************
* 0. Config
********************************************************************************
clear

**** Seta diretorio
cd "Z:\Projects\IEPS Data\Indicadores\"

**** Input
global path_censo_rur     "Data/01_input_data/F_socioeconomicos/F4_censo/censo_rural_urbana.xlsx"
global path_censo_esgoto  "Data/01_input_data/F_socioeconomicos/F4_censo/censo_esgoto.xlsx"
global path_censo_rede    "Data/01_input_data/F_socioeconomicos/F4_censo/censo_rede.xlsx"
global path_censo_fossa   "Data/01_input_data/F_socioeconomicos/F4_censo/censo_fossa.xlsx"
global path_ban_rede      "Data/01_input_data/F_socioeconomicos/F4_censo/censo_rede_banheiro.xlsx"
global path_ban_fossa     "Data/01_input_data/F_socioeconomicos/F4_censo/censo_fossa_banheiro.xlsx"
global path_pop_idade     "Data/02_temp_data/G_demografia/pop_tabnet_idade_2010_2020.dta" 
global path_reg_saude     "Data/02_temp_data/0_auxiliar/Regioes_de_Saude.dta"
global path_macro_saude   "Data/02_temp_data/0_auxiliar/Macrorregioes de Saude.dta"


**** Output
global sf_censo_temp1 "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_temp1.dta"
global sf_censo_temp2 "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_temp2.dta"
global sf_censo_temp3 "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_temp3.dta"
global sf_censo_temp4 "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_temp4.dta"
global sf_censo_temp5 "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_temp5.dta"
global sf_censo_mun   "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_mun.dta"
global sf_censo_reg   "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_reg.dta"
global sf_censo_macro "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_macro.dta"
global sf_censo_uf    "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_uf.dta"
global sf_censo_br    "Data/02_temp_data/F_socioeconomicos/F4_censo/censo_br.dta"


********************************************************************************
* 1. Tratar Dados - Abre e Prepara Dados
********************************************************************************
//-----------------------------------------------------------
***** 1.1. Urbano x Rural 
//-----------------------------------------------------------
clear
import excel using "$path_censo_rur", cellrange(A7:F5571)

**** Descarta variaveis desnecessarias 
drop B C D

**** Renomeia variaveis 
rename E pop_urbana
rename F pop_rural
rename A cod

**** Transforma variavel de populacao rural em numerica
**A tabela do SIDRA não inclui 0 para nenhum município, só "-"
replace pop_rural = "0" if pop_rural=="-"
destring pop_rural, replace

**** Gera codigo de municipio de seis digitos
gen codmun = substr(cod,1,6)
destring codmun, replace
drop cod

order codmun

**** Salva arquivo temporario 1
save "$sf_censo_temp1", replace

//-----------------------------------------------------------
***** 1.2. Esgoto Rede Geral - Sanitario
//-----------------------------------------------------------
clear
import excel using "$path_censo_rede", cellrange(A8:E5572)

**** Descarta variaveis desnecessarias 
drop B C D

**** Renomeia variaveis 
rename E pop_esgoto_rede_san
rename A cod

**** Transforma variavel de populacao rural em numerica
replace pop_esgoto_rede_san = "" if pop_esgoto_rede_san=="-"
destring pop_esgoto_rede_san, replace

**** Gera codigo de municipio de seis digitos
gen codmun = substr(cod,1,6)
destring codmun, replace
drop cod

**** Junta com arquivo temporario 1
merge 1:1 codmun using "$sf_censo_temp1"
drop _merge

**** Salva arquivo temporario 2
save "$sf_censo_temp2", replace

//-----------------------------------------------------------
***** 1.3. Esgoto Fossa - Sanitario
//-----------------------------------------------------------
clear
import excel using "$path_censo_fossa", cellrange(A8:E5572)

**** Descarta variaveis desnecessarias 
drop B C D

**** Renomeia variaveis 
rename E pop_esgoto_fossa_san
rename A cod

**** Transforma variavel de populacao rural em numerica
replace pop_esgoto_fossa_san = "" if pop_esgoto_fossa_san=="-"
destring pop_esgoto_fossa_san, replace

**** Gera codigo de municipio de seis digitos
gen codmun = substr(cod,1,6)
destring codmun, replace
drop cod

**** Junta com arquivo temporario 2
merge 1:1 codmun using "$sf_censo_temp2"
drop _merge

**** Salva arquivo temporario 3
save "$sf_censo_temp3", replace

//-----------------------------------------------------------
**** 1.4. Esgoto Rede - Banheiro
//-----------------------------------------------------------
clear
import excel using "$path_ban_rede", cellrange(A8:E5572)


**** Descarta variaveis desnecessarias 
drop B C D

**** Renomeia variaveis 
rename E pop_esgoto_rede_ban
rename A cod

**** Transforma variavel de populacao rural em numerica
replace pop_esgoto_rede_ban = "" if pop_esgoto_rede_ban=="-"
destring pop_esgoto_rede_ban, replace

**** Gera codigo de municipio de seis digitos
gen codmun = substr(cod,1,6)
destring codmun, replace
drop cod

**** Junta com arquivo temporario 3
merge 1:1 codmun using "$sf_censo_temp3"
drop _merge

**** Salva arquivo temporario 4
save "$sf_censo_temp4", replace

//-----------------------------------------------------------
**** 1.5. Esgoto Fossa - Banheiro
//-----------------------------------------------------------
clear
import excel using "$path_ban_fossa", cellrange(A8:E5572)


**** Descarta variaveis desnecessarias 
drop B C D

**** Renomeia variaveis 
rename E pop_esgoto_fossa_ban
rename A cod

**** Transforma variavel de populacao rural em numerica
replace pop_esgoto_fossa_ban = "" if pop_esgoto_fossa_ban=="-"
destring pop_esgoto_fossa_ban, replace

**** Gera codigo de municipio de seis digitos
gen codmun = substr(cod,1,6)
destring codmun, replace
drop cod

**** Junta com arquivo temporario 1
merge 1:1 codmun using "$sf_censo_temp4"
drop _merge

**** Passa valores missing para zero
replace pop_esgoto_fossa_san = 0 if pop_esgoto_fossa_san==.
replace pop_esgoto_fossa_ban = 0 if pop_esgoto_fossa_ban==.
replace pop_esgoto_rede_san = 0 if pop_esgoto_rede_san==.
replace pop_esgoto_rede_ban = 0 if pop_esgoto_rede_ban==.

**** Gera variavel de populacao com saneamento adequado
gen pop_san_adeq = pop_esgoto_fossa_san+pop_esgoto_fossa_ban+pop_esgoto_rede_san+pop_esgoto_rede_ban

**** Gera variavel de ano (censo de 2010)
gen ano = 2010

**** Ordena variaveis
order ano codmun

********************************************************************************
* 2. Junta bases e agrega para municipio/ano
********************************************************************************

**** Faz o merge com dados de unidades geograficas e de população por idade 
rename codmun ibge
merge m:1 ibge using "$path_reg_saude", keepusing(regiao) nogen
merge m:1 ibge using "$path_macro_saude", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Codigo Regiao de Saude"
la var macrorregiao "Codigo Macrorregiao de Saude"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "Codigo UF"
merge m:1 codmun ano using "${path_pop_idade}", keepusing(pop) 
keep if _merge==3
drop _merge


**** Expandir para criar painel 2010-2020 (repete obs de 2010, unico ano disponivel)
drop ano // exclui antiga coluna de ano
expand 11
by codmun, sort : gen n=_n
gen ano=n
replace ano=2010 if ano==1
replace ano=2011 if ano==2
replace ano=2012 if ano==3
replace ano=2013 if ano==4
replace ano=2014 if ano==5
replace ano=2015 if ano==6
replace ano=2016 if ano==7
replace ano=2017 if ano==8
replace ano=2018 if ano==9
replace ano=2019 if ano==10
replace ano=2020 if ano==11
drop n    // remove coluna temporario

* Save temp	
compress
save "$sf_censo_temp5", replace

**** Gera taxas
gen pct_san_adeq = 100*(pop_san_adeq/pop)
gen pct_rural = 100*(pop_rural/pop)
**** Seleciona variaveis
keep codmun ano pct_rural pct_san_adeq
**** Salvar
save "$sf_censo_mun", replace



********************************************************************************
* 1.3 Agrega para demais niveis geograficos
********************************************************************************

**** Agrega para regiao de saude
use "$sf_censo_temp5", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum)  pop*, by(regiao ano)
**** Gera taxas
gen pct_san_adeq = 100*(pop_san_adeq/pop)
gen pct_rural = 100*(pop_rural/pop)
**** Seleciona variaveis
keep regiao ano pct_rural pct_san_adeq
**** Salvar
save "$sf_censo_reg", replace



**** Agrega para macrorregiao de saude
use "$sf_censo_temp5", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum)  pop*, by(macrorregiao ano)
**** Gera taxas
gen pct_san_adeq = 100*(pop_san_adeq/pop)
gen pct_rural = 100*(pop_rural/pop)
**** Seleciona variaveis
keep macrorregiao ano pct_rural pct_san_adeq
**** Salvar
save "$sf_censo_macro", replace



**** Agrega para UF
use "$sf_censo_temp5", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum)  pop*, by(id_estado ano)
**** Gera taxas
gen pct_san_adeq = 100*(pop_san_adeq/pop)
gen pct_rural = 100*(pop_rural/pop)
**** Seleciona variaveis
keep id_estado ano pct_rural pct_san_adeq
**** Salvar
save "$sf_censo_uf", replace



**** Agrega para o Brasil
use "$sf_censo_temp5", clear
**** Agrupa dados no nível de regiao-ano
collapse (sum) pop*, by(ano)
**** Gera taxas
gen pct_san_adeq = 100*(pop_san_adeq/pop)
gen pct_rural = 100*(pop_rural/pop)
**** Seleciona variaveis
keep ano pct_rural pct_san_adeq
**** Salvar
save "$sf_censo_br", replace



**** Exclui bases intermediarias
*rm "$sf_censo_temp1"
*rm "$sf_censo_temp2"