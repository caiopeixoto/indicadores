*# IEPS DATA
*# Esse codigo serve como masterfile para todos os códigos do IEPS Data, divididos por bloco


*# A - Atencao Primaria ------------------------------------------------------------------------------------------------------------------------------

*# A1 - Cobertura AB


*# Cobertura AB
do "Z:/Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/A_atencao_primaria/A1_cobertura_ab/cobertura_ab.do")

*# Cobertura ACS
do "Z:/Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/A_atencao_primaria/A1_cobertura_ab/cobertura_acs.do")


*# A3 - Prenatal
do "Z:/Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/A_atencao_primaria/A3_prenatal/prenatal.do")



*# B - Mortalidade Morbidade -------------------------------------------------------------------------------------------------------------------------

*# B2 - Mortalidade 

*# Mortalidade
do "Z:/Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/B_mortalidade_morbidade/B2_mort/mort.do")


*# B3 - Morbidade

*# Hospitalizacoes
do "Z:/Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/B_mortalidade_morbidade/B3_hosp/hosp.do")



*# C - Recursos --------------------------------------------------------------------------------------------------------------------------------------

*# Cobertura ACS
do "Z:/Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/C_recursos/recursos.do")



*# D - Saude Suplementar -----------------------------------------------------------------------------------------------------------------------------

*# D1 - Leitos Nao-SUS
do "Z:/Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/D_saude_suplementar/D1_leitos_naosus/leitos_naosus.do")


*# D2 - Cobertura ANS
do "Z:/Projects/IEPS Data/Indicadores/indicadores-code/01_tratar_indicadores/D_saude_suplementar/D2_cobertura_ans/cobertura_ans.do")

