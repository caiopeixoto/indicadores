* IEPS DATA

* Esse codigo trata os dados de cobertura de equipes de Agentes Comunitarios de Saude

														

********************************************************************************
* 0. Config
********************************************************************************

**** Define lista de anos e meses a serem tratados

**** Seta diretorio
cd "Z:/Projects/IEPS Data/"

**** Input 
global path_acs_2007_2018 = "Z:/Projects/IEPS Data/Indicadores/Data/01_input_data/A_atencao_basica/A1_cobertura_ab/Historico-ACS-MUNICIPIOS-2007-2018.xlsx" // dados de 2007-2018
global path_acs_2019 = "Z:/Projects/IEPS Data/Indicadores/Data/01_input_data/A_atencao_basica/A1_cobertura_ab/Cobertura-ACS-TODOS OS MUNICÍPIOS-Dezembro de 2019.xls" // dados de 2019
global path_acs_2020 = "Z:/Projects/IEPS Data/Indicadores/Data/01_input_data/A_atencao_basica/A1_cobertura_ab/Cobertura-ACS-TODOS OS MUNICÍPIOS-Dezembro de 2020.xls" // dados de 2020
global df_healthreg = "Z:/Projects/IEPS Data/Indicadores/Data/auxiliary/Regioes de Saude.dta"
global df_macrohealthreg = "Z:/Projects/IEPS Data/Indicadores/Data/auxiliary/Macrorregioes de Saude.dta"

**** Output 
global sf_cobertura_temp1 "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_temp1.dta"
global sf_cobertura_temp2 "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_temp2.dta"
global sf_cobertura_acs_mun "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_mun.dta"
global sf_cobertura_acs_reg "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_reg.dta"
global sf_cobertura_acs_macro "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_macro.dta"
global sf_cobertura_acs_uf "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_uf.dta"
global sf_cobertura_acs_br "Z:/Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_acs_br.dta"


********************************************************************************
* 1. Tratar Dados - Abre e Prepara Dados
********************************************************************************

**** 1.1. Dados de 2011-2019
import excel "$path_acs_2007_2018", firstrow clear

rename*, lower

**** Mantem somente variaveis necessarias
keep nu_competencia co_municipio_ibge qt_populacao qt_cobertura_acs_ab pc_cobertura_acs_ab

**** Gera variaveis de ano e mes
gen ano = substr(nu_competencia,1,4)
gen mes = substr(nu_competencia,5,6)
destring ano, replace
destring mes, replace
drop nu_competencia

**** Renomeia variaveis 
rename co_municipio_ibge codmun
destring codmun, replace

**** Descarta observacoes referentes a meses que nao dezembro
drop if mes<12
drop mes
drop if ano<2010

**** Transforma variaveis em numericas
destring pc_cobertura_acs_ab, dpcomma replace
rename pc_cobertura_acs_ab cob_acs

destring qt_cobertura_acs_ab, replace ignore(".") dpcomma
destring qt_populacao, replace ignore(.)

**** Salva base intermediaria
save "$sf_cobertura_temp1", replace


**** 1.2. Dados de 2019
clear
import excel "$path_acs_2019", cellrange(A9:I5579) firstrow clear

rename*, lower

**** Mantem somente variaveis necessarias
keep competência ibge população estimpopcobacs coberturaacs

**** Gera variavel de ano
gen ano = substr(competência,5,8)
destring ano, replace
drop competência


**** Transforma variaveis em numericas
destring coberturaacs, dpcomma percent replace
replace coberturaacs = coberturaacs*100
rename coberturaacs cob_acs
rename população qt_populacao
rename estimpopcobacs qt_cobertura_acs_ab
destring qt_cobertura_acs_ab, replace ignore(".") dpcomma
destring qt_populacao, replace ignore(.)

**** Renomeia codigo de municipio de seis digitos
rename ibge codmun
destring codmun, replace

**** Empilha com a base de 2011-2018
append using "$sf_cobertura_temp1"

**** Salva base municipal intermediaria
save "$sf_cobertura_temp1", replace

**** 1.3. Dados de 2020
clear
import excel "$path_acs_2020", cellrange(A9:I5579) firstrow clear

rename*, lower

**** Mantem somente variaveis necessarias
keep competência ibge população estimpopcobacs coberturaacs

**** Gera variavel de ano
gen ano = substr(competência,5,8)
destring ano, replace
drop competência


**** Transforma variaveis em numericas
destring coberturaacs, dpcomma percent replace
replace coberturaacs = coberturaacs*100
rename coberturaacs cob_acs
rename população qt_populacao
rename estimpopcobacs qt_cobertura_acs_ab
destring qt_cobertura_acs_ab, replace ignore(".") dpcomma
destring qt_populacao, replace ignore(.)

**** Renomeia codigo de municipio de seis digitos
rename ibge codmun
destring codmun, replace

**** Empilha com a base de 2011-2019 
append using "$sf_cobertura_temp1"

**** Inclui Legenda para variaveis
label var codmun "Municipality Code"
label var ano "Ano"
label var cob_acs "Community Health Workers Coverage (%)"

**** Junta com base de codigos de regiao e macrorregiao de saude
rename codmun ibge
replace ibge = 530010 if ibge >= 529999
merge m:1 ibge using "$df_healthreg", keepusing(regiao) nogen
merge m:1 ibge using "$df_macrohealthreg", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Health Region Code"
la var macrorregiao "Macro Health Region Code"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "UF Code"

**** Salva base municipal intermediaria
save "$sf_cobertura_temp2", replace

********************************************************************************
* 2. Agrega para niveis geograficos
********************************************************************************

//---------------------------------------------------------------
**** 2.1. Agrega para municipio
//---------------------------------------------------------------
use "$sf_cobertura_temp2", clear

keep codmun ano cob_acs

save "$sf_cobertura_acs_mun", replace

//---------------------------------------------------------------
**** 2.2. Agrega para regiao de saude
//---------------------------------------------------------------

use "$sf_cobertura_temp2", clear

gcollapse (sum) qt_populacao qt_cobertura_acs_ab, by(regiao ano)

**** Calcular coberturas
gen cob_acs = 100*(qt_cobertura_acs_ab/qt_populacao)

keep regiao ano cob_acs

save "$sf_cobertura_acs_reg", replace

//---------------------------------------------------------------
**** 2.3. Agrega para macrorregiao de saude
//---------------------------------------------------------------
use "$sf_cobertura_temp2", clear

gcollapse (sum) qt_populacao qt_cobertura_acs_ab, by(macrorregiao ano)

**** Calcular coberturas
gen cob_acs = 100*(qt_cobertura_acs_ab/qt_populacao)

keep macrorregiao ano cob_acs

save "$sf_cobertura_acs_macro", replace

//---------------------------------------------------------------
**** 2.4. Agrega para UF
//---------------------------------------------------------------
use "$sf_cobertura_temp2", clear

gcollapse (sum) qt_populacao qt_cobertura_acs_ab, by(id_estado ano)

**** Calcular coberturas
gen cob_acs = 100*(qt_cobertura_acs_ab/qt_populacao)

keep id_estado ano cob_acs

save "$sf_cobertura_acs_uf", replace

//---------------------------------------------------------------
**** 2.5. Agrega para Brasil
//---------------------------------------------------------------
use "$sf_cobertura_temp2", clear

gcollapse (sum) qt_populacao qt_cobertura_acs_ab, by(ano)

**** Calcular coberturas
gen cob_acs = 100*(qt_cobertura_acs_ab/qt_populacao)

keep ano cob_acs

save "$sf_cobertura_acs_br", replace

**** Exclui base intermediaria
rm "$sf_cobertura_temp1"