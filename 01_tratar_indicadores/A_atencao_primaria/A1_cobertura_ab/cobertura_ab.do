* IEPS DATA

* Esse codigo trata os dados de cobertura de equipes da Atencao Basica e da Estrategia Saude da Familia

														

********************************************************************************
* 0. Config
********************************************************************************

**** Define lista de anos e meses a serem tratados

*global years `" "2010" "2011" "2012" "2014" "2015" "2016" "2017" "2018" "'
*global months `" "10" "11" "12" "'

**** Define diretorio
**** Define diretorio
cd "Z:/"


**** Input
global path_cobertura_ab_07_20 = "Projects/IEPS Data/Indicadores/Data/01_input_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_2007-2020.xlsx" // dados de cobertura
global df_healthreg = "Projects/IEPS Data/Indicadores/Data/auxiliary/Regioes de Saude.dta"
global df_macrohealthreg = "Projects/IEPS Data/Indicadores/Data/auxiliary/Macrorregioes de Saude.dta"

**** Output
global sf_cobertura_temp "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_temp.dta" // base intermediaria
global sf_cobertura_ab_mun "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_mun.dta"
global sf_cobertura_ab_reg "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_reg.dta"
global sf_cobertura_ab_macro "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_macro.dta"
global sf_cobertura_ab_uf "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_uf.dta"
global sf_cobertura_ab_brasil "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A1_cobertura_ab/cobertura_ab_br.dta"


********************************************************************************
* 1. Tratar Dados - Abre e Prepara Dados
********************************************************************************

**** Empilha dados por estado/ano
clear
tempfile cobertura_ab
save `cobertura_ab', emptyok

import excel using "$path_cobertura_ab_07_20", describe
return list
local n_sheets `r(N_worksheet)'
forvalues j = 1/`n_sheets' {
    local sheet`j' `r(worksheet_`j')'
}

forvalues j = 1/`n_sheets' {
    import excel using "$path_cobertura_ab_07_20", sheet(`"`sheet`j''"') firstrow case(lower) clear
    append using `cobertura_ab'
    save `"`cobertura_ab'"', replace
}

**** Mantem somente variaveis necessarias
keep nu_competencia co_municipio_ibge qt_populacao qt_cobertura_sf qt_cobertura_ab pc_cobertura_ab pc_cobertura_sf


**** Descarta variaveis missing 
drop if nu_competencia==""

**** Cria variaveis de ano e de mes
gen ano = substr(nu_competencia, 1, 4)
gen mes = substr(nu_competencia, 5, 6)

**** Transforma variaveis em numericas
destring ano, replace
destring mes, replace
destring qt_cobertura_sf, replace ignore(.)
destring qt_cobertura_ab, replace ignore(".") dpcomma
destring qt_populacao, replace ignore(.)

**** Descarta variavel de competencia
drop nu_competencia

**** Descarta observacoes referentes a anos abaixo de 2010 
drop if ano<2010
*drop if ano>2019

**** Descarta observacoes referentes a meses que não dezembro
drop if mes<12
drop mes

**** Renomeia variaveis 
rename co_municipio_ibge codmun
rename pc_cobertura_ab cob_ab
rename pc_cobertura_sf cob_esf

**** Transforma variaveis em numericas
destring codmun, replace
destring cob_ab, replace dpcomma percent
destring cob_esf, replace dpcomma percent

**** Inclui Legenda para variaveis
label var codmun "Municipality Code"
label var ano "Ano"
label var cob_ab "Primary Care Coverage"
label var cob_esf "Family Health Strategy Coverage"

**** Junta com base de codigos de regiao e macrorregiao de saude
rename codmun ibge
replace ibge = 530010 if ibge >= 529999
merge m:1 ibge using "$df_healthreg", keepusing(regiao) nogen
merge m:1 ibge using "$df_macrohealthreg", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Health Region Code"
la var macrorregiao "Macro Health Region Code"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "UF Code"

**** Salva base municipal intermediaria
save "$sf_cobertura_temp", replace

********************************************************************************
* 2. Agrega para niveis geograficos
********************************************************************************

********************************************************************************

**** Agrega para municipio/ano
use "$sf_cobertura_temp", clear

keep codmun ano cob_esf cob_ab

save "$sf_cobertura_ab_mun", replace

**** Agrega para regiao de saude
use "$sf_cobertura_temp", clear

gcollapse (sum) qt_populacao qt_cobertura_sf qt_cobertura_ab, by(regiao ano)

**** Calcular coberturas
gen cob_esf = 100*(qt_cobertura_sf/qt_populacao)
gen cob_ab = 100*(qt_cobertura_ab/qt_populacao)

keep regiao ano cob_esf cob_ab

save "$sf_cobertura_ab_reg", replace

**** Agrega para macrorregiao de saude
use "$sf_cobertura_temp", clear

gcollapse (sum) qt_populacao qt_cobertura_sf qt_cobertura_ab, by(macrorregiao ano)

**** Calcular coberturas
gen cob_esf = 100*(qt_cobertura_sf/qt_populacao)
gen cob_ab = 100*(qt_cobertura_ab/qt_populacao)

keep macrorregiao ano cob_esf cob_ab

save "$sf_cobertura_ab_macro", replace

**** Agrega para UF
use "$sf_cobertura_temp", clear

gcollapse (sum) qt_populacao qt_cobertura_sf qt_cobertura_ab, by(id_estado ano)

**** Calcular coberturas
gen cob_esf = 100*(qt_cobertura_sf/qt_populacao)
gen cob_ab = 100*(qt_cobertura_ab/qt_populacao)

keep id_estado ano cob_esf cob_ab

save "$sf_cobertura_ab_uf", replace

**** Agrega para Brasil
use "$sf_cobertura_temp", clear

gcollapse (sum) qt_populacao qt_cobertura_sf qt_cobertura_ab, by(ano)

**** Calcular coberturas
gen cob_esf = 100*(qt_cobertura_sf/qt_populacao)
gen cob_ab = 100*(qt_cobertura_ab/qt_populacao)

keep ano cob_esf cob_ab

save "$sf_cobertura_ab_brasil", replace


**** Exclui base intermediaria
//rm "$sf_cobertura_temp"