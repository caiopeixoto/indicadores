
# IEPS DATA
#
# Esse codigo trata dados de macroregiao de saude de vacinas  



# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------
rm(list=ls())

# Bibliotecas
library(httr)
library(rvest)
library(rlang)
library(rlang)
library(httr)
library(rvest)
library(dplyr)
library(readxl)
library(haven) # Necessario usar este pacote para salvar coluna com muitos caracteres
library(stringr)
library(stringi)


# Setar diretorio
setwd('Z:/Projects/IEPS Data/Indicadores/Data/')



# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------

# Setar ano final a ser baixado
ano_final <- 2020

# Criar dataframe vazio para armazenar dados
vacinas_macro_ano <- NULL

# Loop para Tratar os dados
for (ano in 2010:ano_final) {
  
  # Abrir os dados
  vacinas_macro <- read_xlsx(paste0("01_input_data/A_atencao_basica/A2_cobertura_vacinal/macro_",ano,".xlsx"), skip = 3)
  
  # Adicionar coluna de ano
  vacinas_macro <- vacinas_macro%>%
    mutate(ano = ano)
  
  # Juntar anos
  vacinas_macro_ano <- bind_rows(vacinas_macro_ano, vacinas_macro)
  
}    


# Preparar pra salvar
vacinas_macro_ano <- vacinas_macro_ano %>%
  
  # Renomear colunas
  rename(macrorregiao = "Macrorregião de Saúde",
         cob_vac_bcg = 'BCG',
         cob_vac_rota = 'Rotavírus Humano',
         cob_vac_menin = 'Meningococo C',
         cob_vac_pneumo = 'Pneumocócica',
         cob_vac_polio = 'Poliomielite',
         cob_vac_tvd1 = 'Tríplice Viral  D1',
         cob_vac_penta = 'Penta',
         cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
         cob_vac_hepa = 'Hepatite A')%>%
  
  # Retirar linha de totais
  filter(!is.na(Total),
         macrorregiao != 'Total')%>%
  
  # Mudar separador decimal e criar coluna de codigo municipal
  mutate(macro = as.numeric(substr(macrorregiao, 1, 4)),
         id_estado = as.numeric(substring(macro, first = 1, last = 2)),
         no_macro = substring(macrorregiao, 6),
         no_macro = str_replace(string = no_macro, pattern = "Âª",replacement =  "ª"),
         no_macro = stri_trans_general(tolower(no_macro), "latin-ascii"),
         no_macro = replace(no_macro, no_macro == "macro-roraima", "macro unica - rr"), 
         no_macro = replace(no_macro, no_macro == "macrorregiao de saude sul" & id_estado == 17, "macrorregiao centro-sul"),
         no_macro = replace(no_macro, no_macro == "macrorregiao de saude norte" & id_estado == 17, "macrorregiao norte"),
         
         no_macro = replace(no_macro, no_macro == "macrorregiao centro-sul" & id_estado == 21, "macrorregiao sul"), 
         
         no_macro = replace(no_macro, no_macro == "centro sul" & id_estado == 31, "macrorregiao centro-sul"),
         no_macro = replace(no_macro, no_macro == "sul" & id_estado == 31, "macrorregiao sul"),
         no_macro = replace(no_macro, no_macro == "centro" & id_estado == 31, "macrorregiao centro"),
         no_macro = replace(no_macro, no_macro == "jequitinhonha" & id_estado == 31, "macrorregiao jequitinhonha"),
         no_macro = replace(no_macro, no_macro == "oeste" & id_estado == 31, "macrorregiao oeste"),
         no_macro = replace(no_macro, no_macro == "leste" & id_estado == 31, "macrorregiao leste"),
         no_macro = replace(no_macro, no_macro == "sudeste" & id_estado == 31, "macrorregiao sudeste"),
         no_macro = replace(no_macro, no_macro == "norte" & id_estado == 31, "macrorregiao norte"),
         no_macro = replace(no_macro, no_macro == "noroeste" & id_estado == 31, "macrorregiao noroeste"),
         no_macro = replace(no_macro, no_macro == "leste do sul" & id_estado == 31, "macrorregiao leste do sul"),
         no_macro = replace(no_macro, no_macro == "nordeste" & id_estado == 31, "macrorregiao nordeste"),
         no_macro = replace(no_macro, no_macro == "triangulo do sul" & id_estado == 31, "macrorregiao triangulo do sul"),
         no_macro = replace(no_macro, no_macro == "triangulo do norte" & id_estado == 31, "macrorregiao triangulo do norte"),
         no_macro = replace(no_macro, no_macro == "vale do aco" & id_estado == 31, "macrorregiao vale do aco"),
         
         no_macro = replace(no_macro, no_macro == "central norte" & id_estado == 32, "central"), 
         macro = replace(macro, macro == 3209 & no_macro == "central", 3208),
         
         
         no_macro = replace(no_macro, no_macro == "macrorregiao i" & id_estado == 33, "metropolitana i"), 
         
         cob_vac_bcg = as.numeric(gsub(",",".", cob_vac_bcg)),
         cob_vac_rota = as.numeric(gsub(",",".", cob_vac_rota)),
         cob_vac_menin = as.numeric(gsub(",",".", cob_vac_menin)),
         cob_vac_pneumo = as.numeric(gsub(",",".", cob_vac_pneumo)),
         cob_vac_polio = as.numeric(gsub(",",".", cob_vac_polio)),
         cob_vac_tvd1 = as.numeric(gsub(",",".", cob_vac_tvd1)),
         cob_vac_penta = as.numeric(gsub(",",".", cob_vac_penta)),
         cob_vac_hepb = as.numeric(gsub(",",".", cob_vac_hepb)),
         cob_vac_hepa = as.numeric(gsub(",",".", cob_vac_hepa)))%>%
  select(macro, no_macro, id_estado, ano, cob_vac_bcg, cob_vac_rota, cob_vac_menin, cob_vac_pneumo, cob_vac_polio, cob_vac_tvd1, cob_vac_penta, cob_vac_hepb, cob_vac_hepa) %>% 

  
  # Considerar coberturas acima de 100% como 100%
  mutate_at(.vars=c('cob_vac_bcg','cob_vac_rota','cob_vac_menin','cob_vac_pneumo','cob_vac_polio','cob_vac_tvd1','cob_vac_penta','cob_vac_hepb','cob_vac_hepa'),
            .funs=function(x) {ifelse(x>=100,100,x)})

  
# central norte
vacinas_macro_temp <- vacinas_macro_ano %>% 
  filter(id_estado == 32, macro == 3208, no_macro == "central", ano < 2020)  %>% 
  mutate(no_macro = "norte") %>% 
  mutate(macro = 3206)

vacinas_macro_ano <- bind_rows(vacinas_macro_ano, vacinas_macro_temp) %>% 
  filter(is.na(macro)==FALSE)
  

# ajustes para 2020
vacinas_macro_ano <- vacinas_macro_ano %>%
  distinct(macro, id_estado, ano, .keep_all = TRUE) %>% 
  mutate(no_macro = replace(no_macro, macro == 5302 & ano == 2020, "distrito federal"),
    across(c('cob_vac_bcg','cob_vac_rota','cob_vac_menin','cob_vac_pneumo','cob_vac_polio','cob_vac_tvd1','cob_vac_penta','cob_vac_hepb','cob_vac_hepa'), ~ ifelse(ano == 2020 & macro %in% c(3523, 3518,4308,4309,4310), NA, .))) %>% 
  dplyr::select(-macro)


# Salvar base final
write_dta(vacinas_macro_ano, '02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_macro.dta')


# Fim -----------------------------------------------------------------------------------------------------------------------------------------------