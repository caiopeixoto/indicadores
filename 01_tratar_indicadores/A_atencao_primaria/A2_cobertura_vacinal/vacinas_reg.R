
# IEPS DATA
#
# Esse codigo trata dados de regiao de saude de vacinas  



# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------
rm(list=ls())

# Bibliotecas
library(httr)
library(rvest)
library(rlang)
library(rlang)
library(httr)
library(rvest)
library(dplyr)
library(readxl)


# Setar diretorio
setwd('Z:/Projects/IEPS Data/Indicadores/Data/')



# 1. Tratar Dados -----------------------------------------------------------------------------------------------------------------------------

# Setar ano final a ser baixado
ano_final <- 2020

# Criar dataframe vazio para armazenar dados
vacinas_reg_ano <- NULL

# Loop para tratar os dados
for (ano in 2010:ano_final) {
  
  # Abrir os dados
  vacinas_reg <- read_xlsx(paste0("01_input_data/A_atencao_basica/A2_cobertura_vacinal/reg_",ano,".xlsx"), skip = 3)
  
  # Adicionar coluna de ano
  vacinas_reg <- vacinas_reg%>%
    mutate(ano = ano)
  
  # Juntar anos
  vacinas_reg_ano <- bind_rows(vacinas_reg_ano, vacinas_reg)
  
}    



# Preparar pra salvar
vacinas_reg_ano <- vacinas_reg_ano %>%
  
  # Renomear colunas
  rename(regiao = "Região de Saúde (CIR)",
         cob_vac_bcg = 'BCG',
         cob_vac_rota = 'Rotavírus Humano',
         cob_vac_menin = 'Meningococo C',
         cob_vac_pneumo = 'Pneumocócica',
         cob_vac_polio = 'Poliomielite',
         cob_vac_tvd1 = 'Tríplice Viral  D1',
         cob_vac_penta = 'Penta',
         cob_vac_hepb = 'Hepatite B  em crianças até 30 dias',
         cob_vac_hepa = 'Hepatite A')%>%
  
  # Retirar linha de totais
  filter(!is.na(Total),
         regiao != 'Total')%>%
  
  # Mudar separador decimal e criar coluna de codigo estadual
  mutate(regiao = as.numeric(substr(regiao, 1, 6)),
         cob_vac_bcg = as.numeric(gsub(",",".", cob_vac_bcg)),
         cob_vac_rota = as.numeric(gsub(",",".", cob_vac_rota)),
         cob_vac_menin = as.numeric(gsub(",",".", cob_vac_menin)),
         cob_vac_pneumo = as.numeric(gsub(",",".", cob_vac_pneumo)),
         cob_vac_polio = as.numeric(gsub(",",".", cob_vac_polio)),
         cob_vac_tvd1 = as.numeric(gsub(",",".", cob_vac_tvd1)),
         cob_vac_penta = as.numeric(gsub(",",".", cob_vac_penta)),
         cob_vac_hepb = as.numeric(gsub(",",".", cob_vac_hepb)),
         cob_vac_hepa = as.numeric(gsub(",",".", cob_vac_hepa)))%>%
  select(regiao, ano, cob_vac_bcg, cob_vac_rota, cob_vac_menin, cob_vac_pneumo, cob_vac_polio, cob_vac_tvd1, cob_vac_penta, cob_vac_hepb, cob_vac_hepa) %>% 
  
  # Considerar coberturas acima de 100% como 100%
  mutate_at(.vars=c('cob_vac_bcg','cob_vac_rota','cob_vac_menin','cob_vac_pneumo','cob_vac_polio','cob_vac_tvd1','cob_vac_penta','cob_vac_hepb','cob_vac_hepa'),
            .funs=function(x) {ifelse(x>=100,100,x)})


# Salvar base final
readstata13::save.dta13(vacinas_reg_ano, '02_temp_data/A_atencao_basica/A2_cobertura_vacinal/cov_vac_reg.dta')



# Fim -----------------------------------------------------------------------------------------------------------------------------------------------