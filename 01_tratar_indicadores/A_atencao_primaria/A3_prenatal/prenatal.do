* IEPS DATA

* Esse codigo trata os dados de atendimento pre-natal a partir da base do SINASC

														

********************************************************************************
* 0. Config
********************************************************************************

**** Dicionario de dados disponivel em https://bigdata-metadados.icict.fiocruz.br/dataset/sistema-de-informacoes-de-nascidos-vivos-sinasc/resource/e76fe720-0855-481e-bbb9-18c1028d9501

**** Referencia para lidar com obs missing: https://stats.idre.ucla.edu/stata/modules/missing-values/

**** Referencia para colapsar com obs missing: https://www.statalist.org/forums/forum/general-stata-discussion/general/1504804-handling-of-missing-values-by-the-collapse-command

**** Define lista de anos e estados a serem tratados

clear
global anos = `" "2010" "2011" "2012" "2013"  "2014" "2015" "2016" "2017" "2018" "2019"  "2020" "'
global estados = `" "AC" "AL" "AM" "AP" "BA" "CE" "DF" "ES" "GO" "MA" "MG" "MS" "MT" "PA" "PB" "PE" "PI" "PR" "RJ" "RN" "RO" "RR" "RS" "SC" "SE" "SP" "TO" "' 

**** Define diretorio
cd "Z:/"

**** Input 
global path_sinasc = "Master/Datasus/raw/SINASC/dta/" // dados de nascidos vivos
global df_healthreg = "Projects/IEPS Data/Indicadores/Data/auxiliary/Regioes de Saude.dta"
global df_macrohealthreg = "Projects/IEPS Data/Indicadores/Data/auxiliary/Macrorregioes de Saude.dta"

**** Output
global sf_prenatal_temp "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A3_prenatal/prenatal_temp.dta"
global sf_prenatal_codmun "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A3_prenatal/prenatal_mun.dta"
global sf_prenatal_codreg "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A3_prenatal/prenatal_reg.dta"
global sf_prenatal_codmacro "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A3_prenatal/prenatal_macro.dta"
global sf_prenatal_coduf "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A3_prenatal/prenatal_uf.dta"
global sf_prenatal_br "Projects/IEPS Data/Indicadores/Data/02_temp_data/A_atencao_basica/A3_prenatal/prenatal_br.dta"


********************************************************************************
* 1.1 Tratar Dados - Abre e Prepara Dados
********************************************************************************

**** Empilha dados por estado/ano
clear
gen ano_loop=""
foreach ano of global anos {
	foreach estado of global estados {
		local files: dir "$path_sinasc" files "*`estado'`ano'.dta"
			foreach f of local files {
				di "`estado'"
				di "`ano'"
		
				append using "${path_sinasc}DN`estado'`ano'.dta"

				replace ano_loop="`ano'" if ano_loop==""
				
}				
}
}

**** Mantem somente variaveis necessarias
keep  CODMUNRES CONSPRENAT MESPRENAT ano_loop CONSULTAS

**** Tira capslock dos nomes das variaveis
rename *, lower
rename ano_loop ano

**** Renomeia e transforma codigo de municipio em numerica
rename codmunres codmun
destring codmun, replace
destring ano, replace

**** Imputa o código de Brasilia a municípios que são do DF mas com código ignorado
replace codmun = 530010 if floor(codmun/10000) == 53

**** Considera mesprenat igual a 99 como faltante
replace mesprenat = "" if mesprenat=="99"

**** Transforma variaveis de consultas e mes em numericas
replace consprenat = "" if consprenat=="NA" 
destring consprenat, replace

replace mesprenat = "" if mesprenat=="NA"
destring mesprenat, replace

replace consultas = "" if consultas=="NA"
destring consultas, replace

**** Gera indicador de nascimento
gen byte nnasc = 1

**** Gera indicador de pre-natal adequado
gen prenatal_adeq = (consprenat>=6 & mesprenat<=3) if (consprenat !=. & mesprenat !=.)

**** Passa observacoes da variavel de pre-natal adequado anteriores a 2014 para missing
replace prenatal_adeq=. if ano<2014

**** Descarta variaveis desnecessarias
drop consprenat 
drop mesprenat

*** Gera variaveis de numero de consultas
gen cons_zero = consultas==1 if (consultas !=.)
gen cons_1a6 = (consultas==2 | consultas==3) if (consultas !=.)
gen cons_7m = consultas==4 if (consultas !=.)

********************************************************************************
* 1.2. Tratar Dados - Colapsa para o nivel municipio/ano
********************************************************************************

**** Colapsa dados ao nivel municipio/ano
collapse (sum) nnasc prenatal_adeq cons_zero cons_1a6 cons_7m (count) n_nnasc=nnasc n_prenatal_adeq=prenatal_adeq n_cons_zero=cons_zero n_cons_1a6=cons_1a6 n_cons_7m=cons_7m, by(codmun ano)

**** Transforma observacoes missing em zero
replace nnasc = . if n_nnasc==0
replace prenatal_adeq = . if n_prenatal_adeq==0
replace cons_zero = . if n_cons_zero==0
replace cons_1a6 = . if n_cons_1a6==0
replace cons_7m = . if n_cons_7m==0

**** Descarta variaveis desnecessarias
drop n_*

**** Cria percentuais
gen pct_prenatal_adeq = (prenatal_adeq/nnasc)*100
gen pct_prenatal_zero = (cons_zero/nnasc)*100
gen pct_prenatal_1a6 = (cons_1a6/nnasc)*100
gen pct_prenatal_7m = (cons_7m/nnasc)*100

**** Inclui legendas para as variaveis
label var codmun "Codigo Municipal"
label var pct_prenatal_adeq "Percentual de nascimentos com numero adequado de exames pre natal"
label var pct_prenatal_zero "Percentual de nascimentos com numero zero exames prenatal"
label var pct_prenatal_1a6  "Percentual de nascimentos com 1 a 6 consultas pre natal"
label var pct_prenatal_7m "Percentual de nascimentos com 7 ou mais exames pre natal"

**** Junta com base de codigos de regiao e macrorregiao de saude
rename codmun ibge
merge m:1 ibge using "$df_healthreg", keepusing(regiao) nogen
merge m:1 ibge using "$df_macrohealthreg", keepusing(macrorregiao) nogen
rename ibge codmun
la var regiao "Health Region Code"
la var macrorregiao "Macro Health Region Code"
gen id_estado = real(substr(string(codmun),1,2))
la var id_estado "UF Code"

**** Salva base municipal intermediaria
save "$sf_prenatal_temp", replace

********************************************************************************
* 2. Agrega para niveis geograficos
********************************************************************************

//---------------------------------------------------------------
**** 2.1. Agrega para municipio
//---------------------------------------------------------------
use "$sf_prenatal_temp", clear

**** Selecionar variaveis
keep ano codmun pct_prenatal_adeq pct_prenatal_zero pct_prenatal_1a6 pct_prenatal_7m

save "$sf_prenatal_codmun", replace

//---------------------------------------------------------------
**** 2.2. Agrega para regiao de saude
//---------------------------------------------------------------

use "$sf_prenatal_temp", clear

gcollapse (sum) nnasc prenatal_adeq cons_zero cons_1a6 cons_7m, by(regiao ano)

**** Cria percentuais
gen pct_prenatal_adeq = (prenatal_adeq/nnasc)*100
gen pct_prenatal_zero = (cons_zero/nnasc)*100
gen pct_prenatal_1a6 = (cons_1a6/nnasc)*100
gen pct_prenatal_7m = (cons_7m/nnasc)*100

**** Transforma zero em missing antes de 2014
replace pct_prenatal_adeq =. if ano<2014

**** Selecionar variaveis
keep ano regiao pct_prenatal_adeq pct_prenatal_zero pct_prenatal_1a6 pct_prenatal_7m

save "$sf_prenatal_codreg", replace

//---------------------------------------------------------------
**** 2.3. Agrega para macrorregiao de saude
//---------------------------------------------------------------
use "$sf_prenatal_temp", clear

gcollapse (sum) nnasc prenatal_adeq cons_zero cons_1a6 cons_7m, by(macrorregiao ano)

**** Cria percentuais
gen pct_prenatal_adeq = (prenatal_adeq/nnasc)*100
gen pct_prenatal_zero = (cons_zero/nnasc)*100
gen pct_prenatal_1a6 = (cons_1a6/nnasc)*100
gen pct_prenatal_7m = (cons_7m/nnasc)*100

**** Transforma zero em missing antes de 2014
replace pct_prenatal_adeq =. if ano<2014

**** Selecionar variaveis
keep ano macrorregiao pct_prenatal_adeq pct_prenatal_zero pct_prenatal_1a6 pct_prenatal_7m

save "$sf_prenatal_codmacro", replace

//---------------------------------------------------------------
**** 2.4. Agrega para UF
//---------------------------------------------------------------
use "$sf_prenatal_temp", clear

gcollapse (sum) nnasc prenatal_adeq cons_zero cons_1a6 cons_7m, by(id_estado ano)

**** Cria percentuais
gen pct_prenatal_adeq = (prenatal_adeq/nnasc)*100
gen pct_prenatal_zero = (cons_zero/nnasc)*100
gen pct_prenatal_1a6 = (cons_1a6/nnasc)*100
gen pct_prenatal_7m = (cons_7m/nnasc)*100

**** Transforma zero em missing antes de 2014
replace pct_prenatal_adeq =. if ano<2014

**** Selecionar variaveis
keep ano id_estado pct_prenatal_adeq pct_prenatal_zero pct_prenatal_1a6 pct_prenatal_7m

save "$sf_prenatal_coduf", replace

//---------------------------------------------------------------
**** 2.5. Agrega para Brasil
//---------------------------------------------------------------
use "$sf_prenatal_temp", clear

gcollapse (sum) nnasc prenatal_adeq cons_zero cons_1a6 cons_7m, by(ano)

**** Cria percentuais
gen pct_prenatal_adeq = (prenatal_adeq/nnasc)*100
gen pct_prenatal_zero = (cons_zero/nnasc)*100
gen pct_prenatal_1a6 = (cons_1a6/nnasc)*100
gen pct_prenatal_7m = (cons_7m/nnasc)*100

**** Transforma zero em missing antes de 2014
replace pct_prenatal_adeq =. if ano<2014

**** Selecionar variaveis
keep ano pct_prenatal_adeq pct_prenatal_zero pct_prenatal_1a6 pct_prenatal_7m

save "$sf_prenatal_br", replace
