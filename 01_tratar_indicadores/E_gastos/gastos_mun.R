# IEPS DATA
#
# Esse codigo baixa e trata dados de gastos municipais (SIOPS) 



# 0. Config --------------------------------------------------------------------------------------------------------------------------------------------
rm(list=ls())

# Bibliotecas
library(httr)
library(rlang)
library(rvest)
library(dplyr)
library(readstata13)

# Define diretorio
setwd('Z:/Projects/IEPS Data/Indicadores/Data/')


# Input
path_reg   <-'02_temp_data/0_auxiliar/Regioes de Saude.dta'
path_macro <-'02_temp_data/0_auxiliar/Macrorregioes de Saude.dta'
path_muns  <-'02_temp_data/0_auxiliar/municipios.dta'


# 1. Le arquivos --------------------------------------------------------------------------------------------------------------------------------------------
reg   <-read.dta13(path_reg)
macro <-read.dta13(path_macro)
muns  <-read.dta13(path_muns) %>% 
  dplyr::select(id_munic_6, id_estado, estado_abrev, estado)


# 2. Baixar e Tratar Dados -----------------------------------------------------------------------------------------------------------------------------

# Url dados Municipais
url_mun<-'http://siops-asp.datasus.gov.br/CGI/tabcgi.exe?SIOPS/serhist/municipio/mIndicadores.def'

# Definir ano final a ser baixado
ano_final <- 2020

# Criar dataframe vazio para armazenar dados
dados_mun <- NULL

# Loop para baixar os dados
for (ano in 2010:ano_final) {
 
  print(ano)  # mostra o ano que esta sendo baixado
  ano_loop <- substring(as.character(ano),3,4) # seleciona ultimos digitos do ano 
  
  # Define opcoes do formulario
  form_data_mun <- paste0("Linha=Munic-BR&Coluna=--N%E3o-Ativa--&Incremento=Freq%FC%EAncia&Incremento=Popula%E7%E3o&Incremento=1.1_%25R.L%EDquida_Total&Incremento=1.2_%25Transf._Intergov._l%EDquidas&Incremento=1.3_%25Transf._para_a_Sa%FAde_%28SUS%29&Incremento=1.4_%25Transf._Uni%E3o_p%2F_Sa%FAde&Incremento=1.5_%25Transf._da_Uni%E3o_p%2F_%28SUS%29&Incremento=1.6_%25R.Imp._Transf.Const.Legais&Incremento=2.1_D.Total_Sa%FAde%2FHab&Incremento=2.2_%25D.Pessoal%2FD.Total&Incremento=2.3_%25D.com_Medicamentos&Incremento=2.4_%25D.Serv.Terc%2FD.Total&Incremento=2.5_%25D.Invest%2FD.Total&Incremento=3.1_%25Transf.SUS%2FD.Total&Incremento=3.2_%25R.Pr%F3prios_em_Sa%FAde-EC_29&Incremento=R.Impostos_e_Transf.Const&Incremento=R.Transf.SUS&Incremento=R.Transf.SUS%2FHab&Incremento=D.Pessoal&Incremento=D.R.Pr%F3prios&Incremento=D.R.Pr%F3prios_em_Sa%FAde%2FHab&Incremento=D.Total_Sa%FAde&Arquivos=indmun",
                          ano_loop,
                          ".dbf&SUF=TODAS_AS_CATEGORIAS__&SCapitais=TODAS_AS_CATEGORIAS__&SMunic-BR=TODAS_AS_CATEGORIAS__&SRegi%E3o=TODAS_AS_CATEGORIAS__&SSele%E7%E3o_Capitais=TODAS_AS_CATEGORIAS__&SFaixa_Pop=TODAS_AS_CATEGORIAS__&formato=table&mostre=Mostra")
  
  # Envia o formulario ao site e armazena resultado
  site_mun <- httr::POST(url = url_mun, body = form_data_mun)
  
  # Especifica encoding para comportar caracteres latinos e organiza resultados em tabela
  dados_mun_brutos<- httr::content(site_mun, encoding = "Latin1")%>%
    rvest::html_nodes(xpath='/html/body/center/center/table')%>%
    rvest::html_table(fill=T,trim=T)
  
  # Seleciona a tabela desejada e transforma em data frame
  dados_mun_brutos <- as.data.frame(dados_mun_brutos[[1]])
  
  # Salvar dados brutos
  readstata13::save.dta13(dados_mun_brutos, paste0('01_input_data/E_gastos/siops_mun_', ano,'.dta'))
  
  # Altera decimais
  dados_mun_temp <- dados_mun_brutos %>%
    mutate_at(vars(-`Munic-BR`), gsub,pattern='.',replacement='',fixed=T) %>% # remove pontos marcadores de milhares
    mutate_at(vars(-`Munic-BR`), gsub,pattern=',',replacement='.',fixed=T) # substitui virgulas por pontos
  
  # Renomeia e seleciona variáveis de interesse
  dados_mun_temp <- dados_mun_temp %>% 
    
    # Identifica código do município de 6 digitos e define coluna de ano
    mutate(codmun = substring(`Munic-BR`, 1, 6),
           ano = ano)%>%
    
    # Renomeia e seleciona variáveis
    select(codmun, ano,
           pop_siops                = `População`,
           desp_tot_saude_pc_mun    = `2.1 D.Total Saúde/Hab`,
           pct_desp_recp_saude_mun  = `3.2 %R.Próprios em Saúde-EC 29`,
           desp_recp_saude_pc_mun   = `D.R.Próprios em Saúde/Hab`,
           desp_recp_saude_mun      = `D.R.Próprios`,
           recp_saude_mun           = `R.Impostos e Transf.Const`,
           desp_tot_saude_mun       = `D.Total Saúde`)
  
  # Empilha aos demais anos
  dados_mun <- bind_rows(dados_mun, dados_mun_temp) 
  
}  

# Muda classe de variaveis para numericas
dados_mun <- dados_mun %>% 
  mutate(pop_siops              = as.numeric(pop_siops),
         desp_tot_saude_pc_mun    = as.numeric(desp_tot_saude_pc_mun),
         pct_desp_recp_saude_mun  = as.numeric(pct_desp_recp_saude_mun),
         desp_recp_saude_pc_mun   = as.numeric(desp_recp_saude_pc_mun),
         desp_recp_saude_mun      = as.numeric(desp_recp_saude_mun),
         recp_saude_mun           = as.numeric(recp_saude_mun),
         desp_tot_saude_mun       = as.numeric(desp_tot_saude_mun)) 

# Preparar para salvar
dados_mun_final <- dados_mun %>% 
  filter(codmun != "TOTAL" & codmun != "000000")%>% # Remove linha com totais para o Brasil e codigo desconhecido ("Ignorado ou exterior")
  mutate(codmun = as.numeric(codmun)) %>% 
  dplyr::select(-pop_siops,-desp_tot_saude_mun,-desp_recp_saude_mun,-recp_saude_mun)
# Salvar base municipal
readstata13::save.dta13(dados_mun_final, '02_temp_data/E_gastos/siops_mun.dta')



# 3. AGRUPAR DADOS PARA DEMAIS NIVEIS GEOGRAFICOS ----------------------------------------------------------------------------------------------------------------------

#### Nivel de Regiao de Saude
dados_reg_final <- dados_mun %>% 
  filter(codmun != "TOTAL" & codmun != "000000")%>%  # Remove linha com totais para o Brasil e codigo desconhecido ("Ignorado ou exterior")
  mutate(codmun = as.numeric(codmun)) %>% 
  left_join(reg, by = c('codmun'='ibge')) %>% # junta informacao de regiao de saude 
  dplyr::select(-desp_tot_saude_pc_mun, -pct_desp_recp_saude_mun, -desp_recp_saude_pc_mun, -codmun)  %>% 
  group_by(regiao, ano) %>%
  summarize_if(is.numeric, .funs='sum',na.rm=T) %>% 
  mutate(desp_tot_saude_pc_mun    = desp_tot_saude_mun/pop_siops,
         pct_desp_recp_saude_mun  = 100*(desp_recp_saude_mun/recp_saude_mun),
         desp_recp_saude_pc_mun   = desp_recp_saude_mun/pop_siops) %>% 
  dplyr::select(-pop_siops,-desp_tot_saude_mun,-desp_recp_saude_mun,-recp_saude_mun)

# Salvar base regiao
readstata13::save.dta13(dados_reg_final, '02_temp_data/E_gastos/siops_mun_reg.dta')


#### Nivel de Macrorregiao de Saude
dados_macro_final <- dados_mun %>% 
  filter(codmun != "TOTAL" & codmun != "000000") %>%  # Remover linha com totais para o Brasil
  mutate(codmun = as.numeric(codmun)) %>% 
  left_join(macro, by = c('codmun'='ibge')) %>% # junta informacao de macrorregiao de saude 
  dplyr::select(-desp_tot_saude_pc_mun, -pct_desp_recp_saude_mun, -desp_recp_saude_pc_mun, -codmun) %>% 
  group_by(macrorregiao, ano) %>%
  summarize_if(is.numeric, .funs='sum',na.rm=T) %>% 
  mutate(desp_tot_saude_pc_mun    = desp_tot_saude_mun/pop_siops,
         pct_desp_recp_saude_mun  = 100*(desp_recp_saude_mun/recp_saude_mun),
         desp_recp_saude_pc_mun   = desp_recp_saude_mun/pop_siops)%>% 
  dplyr::select(-pop_siops,-desp_tot_saude_mun,-desp_recp_saude_mun,-recp_saude_mun)

# Salvar base macrorregiao
readstata13::save.dta13(dados_macro_final, '02_temp_data/E_gastos/siops_mun_macro.dta')



#### Nivel UF
dados_uf_final <- dados_mun %>% 
  filter(codmun != "TOTAL" & codmun != "000000")  %>%  # Remover linha com totais para o Brasil
  mutate(codmun = as.numeric(codmun)) %>% 
  left_join(muns, by = c('codmun'='id_munic_6')) %>% # junta informacao de uf
  dplyr::select(-desp_tot_saude_pc_mun, -pct_desp_recp_saude_mun, -desp_recp_saude_pc_mun, -codmun,-estado_abrev, -estado) %>% 
  group_by(id_estado, ano) %>%
  summarize_if(is.numeric, .funs='sum',na.rm=T) %>% 
  mutate(desp_tot_saude_pc_mun    = desp_tot_saude_mun/pop_siops,
         pct_desp_recp_saude_mun  = 100*(desp_recp_saude_mun/recp_saude_mun),
         desp_recp_saude_pc_mun   = desp_recp_saude_mun/pop_siops)%>% 
  dplyr::select(-pop_siops,-desp_tot_saude_mun,-desp_recp_saude_mun,-recp_saude_mun)

# Salvar base uf
write_dta(dados_uf_final, '02_temp_data/E_gastos/siops_mun_uf.dta')



#### Nivel Nacional
dados_br_final <- dados_mun %>% 
  filter(codmun == "TOTAL") %>%    # Manter apenas info nacional
  dplyr::select(-desp_tot_saude_pc_mun, -pct_desp_recp_saude_mun, -desp_recp_saude_pc_mun, -codmun) %>% 
  mutate(desp_tot_saude_pc_mun    = desp_tot_saude_mun/pop_siops, 
         pct_desp_recp_saude_mun  = 100*(desp_recp_saude_mun/recp_saude_mun),
         desp_recp_saude_pc_mun   = desp_recp_saude_mun/pop_siops)%>% 
  dplyr::select(-pop_siops,-desp_tot_saude_mun,-desp_recp_saude_mun,-recp_saude_mun)

# Salvar base br
readstata13::save.dta13(dados_br_final, '02_temp_data/E_gastos/siops_mun_br.dta')



# FIM -----------------------------------------------------------------------------------------------------------------------------------------------